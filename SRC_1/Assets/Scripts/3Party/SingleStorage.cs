﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Remoting.Contexts;


public static class SingleStorage
{
    public static TextReader GetPathToFile(string nameFile)
    {
        //string pathToFileStorage = Application.persistentDataPath + "/" + nameFile + ".xml";
        //string path = nameFile;
        TextReader reader = null;

        /*
#if !UNITY_WEBPLAYER

        if (File.Exists(pathToFileStorage) && Context.DownloadingFile)
        {
            StreamReader streamReader = new StreamReader(pathToFileStorage);
            string text = streamReader.ReadToEnd();
            streamReader.Close();
            path = pathToFileStorage;
            reader = new StringReader(text);
            //Debug.LogError(reader.ToString());
        }
        else
        {
            var txt = Resources.Load(nameFile) as TextAsset;
            reader = new StringReader(txt.text);
        }
#endif

#if UNITY_WEBPLAYER*/
        var txt = Resources.Load(nameFile) as TextAsset;
        if(txt != null)
            reader = new StringReader(txt.text);
//#endif

        return reader;
    }
}
