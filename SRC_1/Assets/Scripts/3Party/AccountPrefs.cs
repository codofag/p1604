using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using PlayerPrefs = OdianGames.PlayerPrefs;

public class AccountPrefs
{
    private const string SoundAvauble = "SOUND_AVAIBLE";
    private const string MusicAvaible = "MUSIC_AVAIBLE";
    private const string PlayerResult = "PLAYERRESULT";

    #region SOUND
    public static bool GetSound()
    {
        return PlayerPrefs.GetBool(SoundAvauble, true);
    }

    public static void SetSound(bool avaible)
    {
        PlayerPrefs.SetBool(SoundAvauble, avaible);
    }
    #endregion

    #region MUSIC
    public static bool GetMusic()
    {
        return PlayerPrefs.GetBool(MusicAvaible, true);
    }

    public static void SetMusic(bool avaible)
    {
        PlayerPrefs.SetBool(MusicAvaible, avaible);
    }
    #endregion

    #region PLAYER RESULT
    public static int GetPlayerResult()
    {
        return PlayerPrefs.GetInt(PlayerResult, 0);
    }

    public static void SetPlayerResult(int r)
    {
        PlayerPrefs.SetInt(PlayerResult, r);
    }
    #endregion

    public static void Save ()
    {
        PlayerPrefs.Flush ();
    }
}