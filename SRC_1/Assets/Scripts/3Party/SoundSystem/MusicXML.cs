﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using System.Collections;
using UnityEngine;

namespace SoundManager.Information
{

    public class MusicXML
    {
        private static StagesDescriptionsXml Root;

        private const string FILE = "XML/Music";

        public static List<MusicsPul> GetMusicByID(string id)
        {
            InitRoot();

            return Root.GetMusicByID(id);
        }

        public static List<MusicsPul> GetMusicByIDAndMissionID(string id, string missionID)
        {
            InitRoot();

            return Root.GetMusicByIDAndMissionID(id, missionID);
        }

        private static void InitRoot()
        {
            if (Root == null)
            {
                TextReader reader = SingleStorage.GetPathToFile(FILE);

                var xmlSerializer = new XmlSerializer(typeof (StagesDescriptionsXml));
                Root = xmlSerializer.Deserialize(reader) as StagesDescriptionsXml;

                reader.Close();
            }
        }

    }

    [XmlRoot("stages-descriptions")]
    public class StagesDescriptionsXml
    {
        [XmlElement("stage-description")] public List<StageDescription> Stage = new List<StageDescription>();

        public StagesDescriptionsXml()
        {
        }

        public List<MusicsPul> GetMusicByID(string id)
        {
            foreach (var stage in Stage)
            {
                if (stage.Id == id)
                {
                    //Debug.LogError("ID := " + id);
                    /*foreach (var stageDescription in stage.Musics)
                {
                    Debug.LogError("weight := " + stageDescription.Weight);
                }*/

                    return stage.Musics;
                }
            }

            return null;
        }

        public List<MusicsPul> GetMusicByIDAndMissionID(string id, string missionId)
        {
            foreach (var stage in Stage)
            {
                if (stage.Id == id && stage.MissionId == missionId)
                {
                    return stage.Musics;
                }
            }

            return null;
        }
    }

    [XmlRoot("stage-description")]
    public class StageDescription
    {
        [XmlAttribute("stage-id")] public string Id;

        [XmlAttribute("mission-id")] public string MissionId;

        [XmlElement("musics-pul")] public List<MusicsPul> Musics = new List<MusicsPul>();

        public StageDescription()
        {
        }
    }

    [XmlRoot]
    public class MusicsPul
    {
        [XmlAttribute("id")] public string Id;

        [XmlAttribute("weight")] public int Weight;

        public MusicsPul()
        {
        }
    }
}