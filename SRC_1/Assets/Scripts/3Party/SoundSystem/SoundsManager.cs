﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using SoundManager.Information;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;


namespace SoundManager
{
    public class SoundsManager : MonoBehaviour
    {
        private List<SoundObject> mvSoundObject;
        private AudioListener mAudioListener;
        private GameObject mAudioGameObject;

        public static SoundsManager Instance;

        private string mPlayedMusicId;
        private SoundObject MusicObject;

        private string mPlayedAmbientId;
        private SoundObject AmbientObject;

        public void Init()
        {
            //Debug.LogError("Init sound manager");

            if (Instance == null)
            {
                Instance = this;
                mvSoundObject = new List<SoundObject>();

                if (mAudioGameObject == null)
                {
                    mAudioGameObject = new GameObject();
                    mAudioGameObject.name = "AudioSource";
                    DontDestroyOnLoad(mAudioGameObject);
                }
                
                mPlayedMusicId = "";
            }

            //SceneManager.activeSceneChanged += OnSceneWasSwitched;
            SceneManager.sceneLoaded += OnSceneWasChanged;
        }

        public SoundObject AddSound(GameObject go, string Id, float timeToBegin = 0.0f, float pitch = 1.0f)
        {
            if (!AccountPrefs.GetSound())
                return null;

            if (!string.IsNullOrEmpty(Id) && SoundManagerXML.ExistSoundMusic(Id))
            {
                //Debug.LogError("omfg := " + Id);
                var ctrl = go.AddComponent<SoundObject>();
                ctrl.mAudioSource = go.AddComponent<AudioSource>();
                ctrl.mAudioSource.rolloffMode = AudioRolloffMode.Linear;
                ctrl.mAudioSource.maxDistance = 200;
                ctrl.mAudioSource.minDistance = 3;
                ctrl.mGameObject = go;

                //Debug.LogError("time := " + timeToBegin + " pitch := " + pitch);

                ctrl.Init(Id);
                ctrl.mTimeToBegin = timeToBegin;
                ctrl.mAudioSource.pitch = pitch;
                ctrl.SetMaxVolume(1.0f);
                
                mvSoundObject.Add(ctrl);

                return ctrl;
            }
            return null;
        }

        public void SetMusicVolume(float volume)
        {
            foreach (var soundObject in mvSoundObject)
            {
                if (soundObject.mTypeSound == SoundObject.TypeSound.MUSIC)
                {
                    soundObject.SetMaxVolume(volume);
                }
            }

            if(AmbientObject != null)
                AmbientObject.SetMaxVolume(volume);
        }

        public void SetAmbientVolume(float volume)
        {
            foreach (var soundObject in mvSoundObject)
            {
                if (soundObject.mTypeSound == SoundObject.TypeSound.AMBIENT)
                {
                    soundObject.SetMaxVolume(volume);
                }
            }
        }


        public void DelSound(GameObject go, string Id)
        {
            foreach (var soundObject in mvSoundObject)
            {
                if (soundObject.mId == Id && soundObject.mGameObject == go)
                {
                    soundObject.Delete();
                }
            }
        }

        public void PlaySound(SoundObject soundObject)
        {
            foreach (var sound in mvSoundObject)
            {
                if (sound == soundObject)
                {
                    soundObject.Playing();
                }
            }
        }

        public void AddAmbient(string typeMission)
        {
            var musics = MusicXML.GetMusicByIDAndMissionID("AMBIENT", typeMission);
            if (musics == null)
                musics = MusicXML.GetMusicByIDAndMissionID("AMBIENT", "DEFAULT_SET");

            var Id = GetIDForWeight(musics);

            //Debug.LogError("Ambient := " + Id);

            if (!string.IsNullOrEmpty(Id) && SoundManagerXML.ExistSoundMusic(Id))
            {
                var ctrl = mAudioGameObject.AddComponent<SoundObject>();
                //OffLoopSounds();
                ctrl.mAudioSource = mAudioGameObject.AddComponent<AudioSource>();
                ctrl.Init(Id);
                ctrl.SetMaxVolume(AccountPrefs.GetMusic() ? 0.5f : 0.0f);
                ctrl.mFadeIn = false;
                ctrl.mFadeOut = false;
                //ctrl.SetMaxVolume(1);

                ctrl.Playing();
                ctrl.mIsLooping = true;
                ctrl.mIsDeletePlaySound = false;
                ctrl.OnActionEndMusic = OnActionEndAmbient;
                ctrl.mTypeSound = SoundObject.TypeSound.AMBIENT;

                mvSoundObject.Add(ctrl);
            }
        }

        public void AddMusic(string Id)
        {
            if (!string.IsNullOrEmpty(Id) && SoundManagerXML.ExistSoundMusic(Id))
            {
                if (mPlayedMusicId == Id)
                    return;

                //Debug.LogError("Add Music Id:= " + Id + " current music id := " + mPlayedMusicId);

                var ctrl = mAudioGameObject.AddComponent<SoundObject>();
                //OffLoopSounds();
                ctrl.mAudioSource = mAudioGameObject.AddComponent<AudioSource>();
                ctrl.Init(Id);
                ctrl.SetMaxVolume(AccountPrefs.GetMusic() ? 0.5f : 0f);
                ctrl.mFadeIn = false;
                ctrl.mFadeOut = false;
                //ctrl.SetMaxVolume(1);

                ctrl.Playing();
                ctrl.OnActionEndMusic = OnActionEndMusic;

                if (mPlayedMusicId != "")
                {
                    Debug.LogError("Delete Music := " + mPlayedMusicId);
                    DelMusic(mPlayedMusicId);
                }

                mvSoundObject.Add(ctrl);

                mPlayedMusicId = Id;

                //Debug.LogError("Add Music Id := " + mPlayedMusicId);
                MusicObject = ctrl;
            }
        }

        private void OnActionEndMusic(SoundObject soundObject)
        {
            //Debug.LogError("On Action End Music := " + soundObject.mId);

            if(!soundObject.mIsDelete)
                soundObject.SoundDown();

            mPlayedMusicId = "";
            ChoiseNewMusic();
        }

        private void OnActionEndAmbient(SoundObject soundObject)
        {
            Debug.Log("On Action End Ambient := " + soundObject.mId);

            if (!soundObject.mIsDelete)
                soundObject.SoundDown();

            mPlayedAmbientId = "";
        }

        public void DelAmbient()
        {
            foreach (var soundObject in mvSoundObject)
            {
                if(soundObject.mTypeSound == SoundObject.TypeSound.AMBIENT)
                    soundObject.SoundDown();
            }
        }

        public void DelMusic(string Id)
        {
            //Debug.LogError("FIND " + Id);
            foreach (var soundObject in mvSoundObject)
            {
                if (soundObject.mId == Id)
                {
                    //Debug.Log("FIND");
                    soundObject.SoundDown();
                }
            }
        }

        public void CheckAudioListener()
        {
            var audioListener = (AudioListener)FindObjectOfType(typeof(AudioListener));
            //Debug.LogError("audio := " + audioListener);
            if (audioListener != null)
            {
                mAudioGameObject.transform.position = audioListener.transform.position;
            }
        }

        public void Update()
        {
            for (int i = 0; i < mvSoundObject.Count; i++)
            {
                if (mvSoundObject[i].mIsDelete)
                {
                    //Debug.LogError("Delete Music := ");
                    var sound = mvSoundObject[i];
                    mvSoundObject.Remove(sound);
                    Destroy(sound.mAudioSource);
                    Destroy(sound);
                    i = 0;
                }
            }

            foreach (var soundObject in mvSoundObject)
            {
                if (soundObject.mIsPlaying)
                {
                
                }
                else
                {
                    // запускаем не играющий звук с опозданием
                    soundObject.mCurentTime += Time.deltaTime;

                    if (soundObject.mTimeToBegin <= soundObject.mCurentTime)
                    {
                        if (soundObject.mTypeSound == SoundObject.TypeSound.MUSIC)
                        {
                            //PlayMusic(soundObject);
                        }
                        else
                        {
                            //Debug.Log("Play sound");
                            PlaySound(soundObject);
                        }
                    }
                }
            }   
        }

        private void OnSceneWasChanged(Scene scene, LoadSceneMode loadSceneMode)
        {
            if (loadSceneMode == LoadSceneMode.Additive)
                return;

            CheckAudioListener();
            
            DelAmbient();

            var musics = MusicXML.GetMusicByID(scene.name);

            if (scene.name == "Game")
            {
                DelMusic(mPlayedMusicId);

                musics = MusicXML.GetMusicByIDAndMissionID(scene.name, "DEFAULT_SET");
            }

            if (musics == null)
            {
                if (MusicObject != null)
                {
                    MusicObject.mFadeOut = true;
                    MusicObject.mIsActionEnd = false;
                    MusicObject = null;
                    mPlayedMusicId = "";
                }
                return;
            }

            bool find = false;
            foreach (var musicsPul in musics)
            {
                if (musicsPul.Id == mPlayedMusicId)
                {
                    find = true;
                    break;
                }
            }

            if (find)
                return;

            if (MusicObject != null)
            {
                MusicObject.mFadeOut = true;
                MusicObject.mIsActionEnd = false;
                MusicObject = null;
                mPlayedMusicId = "";
            }

            ChoiseNewMusic();
        }

        void ChoiseNewMusic()
        {
            var musics = MusicXML.GetMusicByID(SceneManager.GetActiveScene().name);

            //Debug.LogError("Application.loadedLevelName := " + Application.loadedLevelName);

            if (SceneManager.GetActiveScene().name == "Game")
            {
                musics = MusicXML.GetMusicByIDAndMissionID(SceneManager.GetActiveScene().name, "DEFAULT_SET");
            }

            if (musics == null)
                return;

            var melodi = GetIDForWeight(musics);
            //Debug.LogError("Melodi := " + melodi);
            AddMusic(melodi);
        }

        public string GetIDForWeight(List<MusicsPul> mvMusicPull)
        {
            mvMusicPull.Sort((emp1, emp2) => emp1.Weight.CompareTo(emp2.Weight));  
            
            float weightProc = 0;
            foreach (var musicsPul in mvMusicPull)
            {
                weightProc += musicsPul.Weight;
                //Debug.LogError("weight := " + musicsPul.Weight);
            }

            weightProc = 1.0f / weightProc;

            var randomNumber = Random.Range(0.0f, 1.0f);

            //Debug.LogError("random := " + randomNumber + " weight := " + weightProc);

            var weight = 0.0f;
            string melodi = mvMusicPull.Count > 0 ? mvMusicPull[0].Id : "";

            //Debug.LogError("deb");
            foreach (var musicsPul in mvMusicPull)
            {
                weight += musicsPul.Weight * weightProc;

                //Debug.LogError("weight := " + weight);

                if (weight >= randomNumber)
                {
                    melodi = musicsPul.Id;
                    //Debug.LogError("melodi := " + melodi);
                    break;
                }
            }

           // Debug.LogError("melodi := " + melodi);

            return melodi;
        }
    }


}

