﻿using System.Linq;
using UnityEngine;
using System.Collections;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.IO;
using System;

namespace SoundManager.Information
{
    public class SoundManagerXML
    {
        //
        public const string INFORMATION_FILE = "XML/SoundManagerXML";

        private static InformationsSoundlXml mXml;
        
        public static InformationsSoundlXml MXml
        {
            get { return mXml; }
        }

        public static void Init(bool isReloaded)
        {
            if (mXml == null || isReloaded)
            {
                
                TextReader reader = SingleStorage.GetPathToFile(INFORMATION_FILE);

                var xmlSerializer = new XmlSerializer(typeof (InformationsSoundlXml));
                mXml = xmlSerializer.Deserialize(reader) as InformationsSoundlXml;
                reader.Close();
            }
        }

        public static InformationSoundXml GetSound(string Id)
        {
            Init(false);

            return mXml.Get(Id);
        }

        public static bool ExistSoundMusic(string Id)
        {
            Init(false);
            return mXml.Get(Id) != null;
        }

        public static InformationSoundXml GetSounfByName(string nameSound)
        {
            Init(false);
            return mXml.GetByName(nameSound);
        }
    }

    [XmlRoot("Sounds")]
    public class InformationsSoundlXml
    {
        [XmlElement("Sound")] public List<InformationSoundXml> InformationSounds = new List<InformationSoundXml>();

        public InformationsSoundlXml()
        {
        }

        public InformationSoundXml Get(string Id)
        {
            var info = InformationSounds.Find(go => go.id == Id);

            //Debug.LogError("id:= " + Id + " info sound name := " + info.path);

            return info;
        }

        public InformationSoundXml GetByName(string name)
        {
            //Debug.LogError("Name := " + name);

            foreach (var informationSoundXml in InformationSounds)
            {
                var path = informationSoundXml.GetSoundPath();
                if(path == null)
                    continue;

                var index = path.IndexOf("/");
                var substring = path.Substring(index + 1);
                if (substring.Contains(name))
                {
                    return informationSoundXml;
                }
            }

            //Debug.LogError("return NULL");

            return null;

           // var info = InformationSounds.Find(go => go.GetSoundPath().Substring(go.GetSoundPath().LastIndexOf("/") + 1).Contains(name));

           // return info;
        }
    }

    [XmlRoot("Sound")]
    public class InformationSoundXml
    {
        [XmlAttribute("type")] public string type;
        //
        [XmlAttribute("id")] public string id;

        [XmlAttribute("path")] public string path;

        [XmlAttribute("surround")] public bool Surround;

        [XmlAttribute("loop")] public string loop;

        [XmlAttribute("fadein")] public string fadein;

        [XmlAttribute("fadeout")] public string fadeout;

        [XmlAttribute("timeToBegin")] public string timeToBegin;

        [XmlElement("ru")] public string Ru;

        [XmlElement("en")] public string En;

        public InformationSoundXml()
        {
        }

        public string GetSoundPath()
        {
            return path;
           
            /*
            if (string.IsNullOrEmpty(path))
            {
                return (Context.Language == SupportLanguage.Russian) ? Ru : En;		
            }
             */

            //return path;
        }
    }
}