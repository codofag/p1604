﻿using System;
using System.IO;
using UnityEngine;
using System.Collections;
using SoundManager.Information;

namespace SoundManager
{
    public class SoundObject : MonoBehaviour
    {
        public enum TypeSound 
        {
            AMBIENT,
            MUSIC,
            SOUND
        }

        // за какое время выходим / уходи в fade
        private float mTimeFadeIn = -1f;
        private float mTimeFadeOut = -1f;

        // надо ли уходить
        public bool mFadeIn = false;
        public bool mFadeOut = false;
    
        // максимальная громкость
        private float mMaxVolume = 1f;
        //private float mCurrentMaxVolume = 1f;

        public AudioSource mAudioSource = null;

        public GameObject mGameObject = null;

        public TypeSound mTypeSound =  TypeSound.MUSIC;

        public bool mIsLooping = false;

        public string mId = "";
        public string mPathToFile = "";

        public float mTimeToBegin = -1f;
        public float mCurentTime = 0f;

        public bool mIsPlaying = false;

        public bool mIsDelete = false;

        public bool mIsDeletePlaySound = false;

        public Action<SoundObject> OnActionEndMusic;
        public Action<SoundObject> OnActionAndAmbient;

        public bool mIsActionEnd;

        public void Init(string id)
        {
            

            var sound = SoundManagerXML.GetSound(id);
            mId = sound.id;
            mPathToFile = sound.GetSoundPath();

            mIsLooping = sound.loop != null;
            mTimeToBegin = (float)Convert.ToDouble(sound.timeToBegin);// sound.timeToBegin;

            //Debug.Log("time to begin " + mTimeToBegin);

            mTimeFadeIn = (float)Convert.ToDouble(sound.fadein);
            mTimeFadeOut = (float) Convert.ToDouble(sound.fadeout);

            //Debug.Log("in " + mTimeFadeIn + " out " + mTimeFadeOut);

            mCurentTime = 0f;

            mIsDelete = false;
            mIsActionEnd = true;

            //Debug.LogError("path := " + mPathToFile + " id:= " + id + " sound Id := " + mId);

            mAudioSource.clip = (AudioClip)Resources.Load(mPathToFile);

            mTypeSound = sound.type.ToLower() == "music" ? TypeSound.MUSIC : TypeSound.SOUND;

            /*
            if (mTypeSound == TypeSound.MUSIC || mTypeSound == TypeSound.AMBIENT)
            {
                Debug.LogError("Init Sound := " + id + " fadeOut := " + mFadeOut);
            }*/
        }

        public void Playing()
        {
            if (mAudioSource == null)
                return;

            mIsPlaying = true;
           
            mAudioSource.Play();
            mAudioSource.loop = mIsLooping;

            mAudioSource.volume = mMaxVolume;

            if (mTimeFadeIn > 0)
            {
                SoundUp();
                mAudioSource.volume = 0;
            }

            if (mTypeSound == TypeSound.SOUND)
                mIsDeletePlaySound = true;
        }

        public void SetMaxVolume(float volume)
        {
            mMaxVolume = volume;
        }

        public void Delete()
        {
            mIsDelete = true;
        }

        public void SoundUp()
        {
            mFadeIn = true;
            mFadeOut = false;
        }

        public void SoundDown()
        {
            mFadeIn = false;
            mFadeOut = true;
        }

        public void Update()
        {
           
            if (!mIsPlaying)
                return;

            if (mAudioSource.time == 0 && !mAudioSource.isPlaying && !mIsDeletePlaySound)
            {
                mAudioSource.Play();
            }

            
            /*if ((mAudioSource.clip.length - mAudioSource.time) <= mTimeFadeOut)
                Debug.LogError("ID := " + mId + " Time := " + mAudioSource.time + " TimeSample := " + mAudioSource.clip.length + " is Action := " + mIsActionEnd + " lopp := " + mIsLooping);/**/
            //Debug.Log("Init Sound := " + mId + " fadeOut := " + mFadeOut);

            if ( (mTypeSound == TypeSound.MUSIC || mTypeSound == TypeSound.AMBIENT ) && mIsActionEnd && !mIsLooping )
            {
                if ((mAudioSource.clip.length - mAudioSource.time) <= mTimeFadeOut)
                {
                    mIsActionEnd = false;

                    if (mTypeSound == TypeSound.MUSIC)
                    {
                        if (OnActionEndMusic != null)
                            OnActionEndMusic(this);
                    }
                    else if(mTypeSound == TypeSound.AMBIENT)
                    {
                        if (OnActionAndAmbient != null)
                            OnActionAndAmbient(this);
                    }

                    
                }
            }

            if (mIsDeletePlaySound && (mAudioSource == null || mGameObject == null))
            {
                Delete();
                return;
            }

            if (mIsDeletePlaySound && (mAudioSource == null || !mGameObject.activeInHierarchy))
            {
                Delete();
                return;
            }
  
            // если звук доиграл помечаем его как удаленныйы
            if (mIsDeletePlaySound)
            {
                if (!mAudioSource.isPlaying)
                {
                    mIsDeletePlaySound = false;
                    mIsDelete = true;
                }
            }

            if (mFadeIn)
            {
                if (mAudioSource.volume < mMaxVolume)
                {
                    //Debug.Log("volume " + mAudioSource.volume);
                    mAudioSource.volume = mAudioSource.volume + (Time.deltaTime / (mTimeFadeIn + 1));

                    if (mAudioSource.volume >= mMaxVolume)
                    {
                        mAudioSource.volume = mMaxVolume;
                        mFadeIn = false;
                    }
                }
            }
            else if(mFadeOut)
            {
                Debug.Log("Fadeout " + mAudioSource.volume);
                if (mAudioSource.volume > 0)
                {
                    //Debug.Log("volume " + mAudioSource.volume);
                    mAudioSource.volume = mAudioSource.volume - (Time.deltaTime / (mTimeFadeOut + 1));

                    if (mAudioSource.volume <= 0)
                    {
                        mAudioSource.volume = 0;
                        mFadeOut = false;
                        mIsPlaying = false;
                        mIsDelete = true;

                        if (mIsActionEnd)
                        {
                            Debug.Log("end := " + mId);

                            mIsActionEnd = false;

                            if (mTypeSound == TypeSound.MUSIC)
                            {
                                if (OnActionEndMusic != null)
                                    OnActionEndMusic(this);
                            }
                            else if (mTypeSound == TypeSound.AMBIENT)
                            {
                                if (OnActionAndAmbient != null)
                                    OnActionAndAmbient(this);
                            }
                        }
                    }
                }
            }
        }
    }
}

