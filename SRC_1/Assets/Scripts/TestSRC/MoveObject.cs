﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;


public class MoveObject : MonoBehaviour   
{
    public GameObject Target;

    private int NumberWave = 0;
    private int MaxNumberWave = 5;

    private List<Vector3> points = new List<Vector3>();

    // Use this for initialization
    void Start ()
    {
        points = GenerationPoint(transform.position, Target.transform.position);
    }
	
	// Update is called once per frame
	void Update ()
    {
        for (int i = 1; i < points.Count; i++)
        {
            Debug.DrawLine(points[i-1], points[i]);
        }
    }

    //-2 -2 

    private List<Vector3> GenerationPoint(Vector3 pointStart, Vector3 pointEnd)
    {
        List<Vector3> p = new List<Vector3>();

        p.Add(pointStart);  

        float r = 3;
       
        for (int i = 0; i < MaxNumberWave - NumberWave; i++)
        {
            var pos = Vector3.Lerp(pointStart, pointEnd, (1.0f/(MaxNumberWave - NumberWave))*i);
            var z = Random.Range(pos.z-r, pos.z+r);
            var x = Random.Range(pos.x - r, pos.x + r);

            //Debug.Log(" x := " + x + " z := " + z);

            p.Add(new Vector3(x, 0, z));
        }

        p.Add(pointEnd);

        return p;
    }
}
