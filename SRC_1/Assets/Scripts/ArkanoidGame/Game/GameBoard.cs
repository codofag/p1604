﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace ArcanoidGame
{

    public class GameBoard : MonoBehaviour
    {
        public EdgeCollider2D GameBoardEdgeCollider2D;
        public Text ScoresText;

        private float _WidthGameBoard;
        private float _HeightGameBoard;
        private float _ScaleGameBoard;

        private List<Vector2> _PhisicsPoints = new List<Vector2>();

        public Action<GameObject> OnActionColision;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void SetScoresText(float scores)
        {
            ScoresText.text = "" + scores;
        }

        public void Init()
        {
            var s = GetComponent<RectTransform>().sizeDelta;
            _WidthGameBoard = s.x;
            _HeightGameBoard = s.y;

            _ScaleGameBoard = 1.0f;

            _PhisicsPoints.AddRange(GameBoardEdgeCollider2D.points);

            SetScaleBoard(_ScaleGameBoard);
        }

        public Vector2 GetSizeGameBoard()
        {
            return GetComponent<RectTransform>().sizeDelta;
        }

        public void SetScaleBoard(float scale)
        {
            _ScaleGameBoard = scale;
            var w = _WidthGameBoard*_ScaleGameBoard;

            GetComponent<RectTransform>().sizeDelta = new Vector2(w, _HeightGameBoard);

            var points = GameBoardEdgeCollider2D.points;
            var offset = new Vector2(w - _WidthGameBoard, 0);
            for (int i = 7; i < points.Length; i++)
            {
                points[i] = _PhisicsPoints[i] + offset;
            }

            GameBoardEdgeCollider2D.points = points;
        }

        void OnCollisionEnter2D(Collision2D coll)
        {
            //Debug.Log("coolision := " + coll.gameObject.GetComponentInParent<GameFlyObject>().GetTypeFlyObject());

            if (OnActionColision != null)
            {
                var go = coll.gameObject.GetComponentInParent<GameFlyObject>();
                if (go != null)
                {
                    OnActionColision(go.gameObject);
                }
            }
        }
    }
}