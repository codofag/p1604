﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameLive : MonoBehaviour
{
    public Image GameLive1;
    public Image GameLive2;
    public Image GameLive3;

    private float _gameLiveCount = 3;

    public Action<GameObject> OnActionGameOver;

    // Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    void UpdateGameLive()
    {
        float f1, f2, f3;
        f1 = f2 = f3 = _gameLiveCount;// >= 1.0f ? 1.0 : _gameLiveCount;
        f1 = _gameLiveCount >= 1.0f ? 1.0f : _gameLiveCount >= 1.0 ? 1.0f : _gameLiveCount;

        f2 -= 1.0f;

        f2 = f2 <= 0 ? 0 : f2 >= 1.0f ? 1.0f : f2;


        f3 -= 2.0f;

        f3 = f3 <= 0 ? 0 : f3 >= 1.0f ? 1.0f : f3;

        GameLive1.fillAmount = f1;
        GameLive2.fillAmount = f2;
        GameLive3.fillAmount = f3;
    }
     
    public void IncrimentLive()
    {
        if (_gameLiveCount < 0)
            _gameLiveCount = 0;

        _gameLiveCount += 0.2f;

        if (_gameLiveCount >= 3.0f)
            _gameLiveCount = 3.0f;

        UpdateGameLive();
    }

    public void DecrementLive()
    {
        _gameLiveCount-= 1.0f;

        UpdateGameLive();

        if (_gameLiveCount <= -1)
        {
            if (OnActionGameOver != null)
                OnActionGameOver(gameObject);
        }


    }
}
