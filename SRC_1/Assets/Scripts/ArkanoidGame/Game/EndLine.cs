﻿using System;
using UnityEngine;
using System.Collections;
using ArcanoidGame;

public class EndLine : MonoBehaviour
{
    public Action<GameObject> OnActionColision;

    // Use this for initialization
    void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (OnActionColision != null)
        {
            var go = coll.gameObject.GetComponentInParent<GameFlyObject>();
            if (go != null)
            {
                OnActionColision(go.gameObject);
            }
        }
    }
}
