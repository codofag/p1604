﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Game.UI;
using SoundManager;
using Random = UnityEngine.Random;

namespace ArcanoidGame
{

    public class GameLoop : MonoBehaviour
    {
        public Canvas CanvasGame;
        
        public GameObject Root;

        public GameFlyObject GameFlyObjectPrefab;
        public GameBoard GameBoardPrefab;

        public GameLive GameLive;

        public EndLine EndLineController;

        private Vector3 _NowPosition = Vector3.zero;
        
        private float _speedObject = 300.0f;
        private float _speedBoard = 50.0f;

        private Vector3 _sizeBoard;
        private Vector3 _sizeCanvas;
        private Vector3 _scaleCanvas;
        private Vector3 _halfSizeBoard;

        private float _TimeRespawn = 2.0f;
        private float _CurentTimeRespawn = 0.0f;

        private int _scores = 0;

        private bool _isDestroyPause = false;
        private bool _isPause = false;

        private float _timeToPause = 3.0f;
        private float _curTimeToPause = 0.0f;

        private float _timeToFlash = 0.2f;
        private float _curTimeToFlash = 0.0f;

        private float _timeToDestroyFlyObject = 0.2f;
        private float _curTimeToDestroyFlyObject = 0.0f;

        private List<GameObject> _deleteObejct = new List<GameObject>();

        private bool _isPlayingSoundMoveBoard = false;
        private float _timeToMovie = 0.0f;

        // Use this for initialization
        void Start()
        {
            _NowPosition = CanvasGame.GetComponent<RectTransform>().sizeDelta/2;

            GameLive.OnActionGameOver = OnActionGameOver;

            GameBoardPrefab.OnActionColision = OnActionColision;

            Screen.orientation = ScreenOrientation.Portrait;

            EndLineController.OnActionColision = OnActionColisionEndLine;

            if (SoundsManager.Instance != null)
                SoundsManager.Instance.AddSound(gameObject, "START_GAME_ARKANOID");

            if (UIManager.Instance != null)
            {
                UIManager.Instance.OnActionPauseGame = OnActionPauseGame;
                UIManager.Instance.CreateUI();
                UIManager.Instance.CreateTypeUI(EnumUIType.UpRightSettinfButton);
            }

            GameBoardPrefab.SetScoresText(_scores);
        }

        private void OnActionPauseGame(bool pause)
        {
            var gos = Root.GetComponentsInChildren<GameFlyObject>();

            foreach (var gameFlyObject in gos)
            {
                gameFlyObject.SetPause(pause);
            }

            _isPause = pause;
        }

        private void OnActionColisionEndLine(GameObject gameObject)
        {
            var flyObject = gameObject.GetComponent<GameFlyObject>();

            if (flyObject == null)
                return;

            if (flyObject.GetTypeFlyObject() == EnumTypeFlyObject.Telephone)
            {
                Debug.Log("Dicrement one live ");
                GameLive.DecrementLive();
                Handheld.Vibrate();
                DescretingOneLive();
            }
        }


        // Update is called once per frame
        void Update()
        {
            if (_isPause)
                return;

            UpdateTime();
            UpdateInput();
            UpdateTimePause();
        }

        private void OnActionGameOver(GameObject go)
        {
            if (UIManager.Instance != null)
            {
                UIManager.Instance.CreateTypeUI(EnumUIType.EndGameMenu);
                UIManager.Instance.SetUIValue(EnumUIType.EndGameMenu, _scores);
            }

            if (SoundsManager.Instance != null)
                SoundsManager.Instance.AddSound(gameObject, "END_GAME_ARKANOID");

            _TimeRespawn = 5000;
        }

        private void UpdateTimePause()
        {
            if (!_isDestroyPause)
                return;

            _curTimeToPause += Time.deltaTime;
            if (_curTimeToPause >= _timeToPause)
            {
                _isDestroyPause = false;
                _curTimeToFlash = 1.0f;
                GameBoardPrefab.transform.gameObject.SetActive(true);
                return;
            }

            _curTimeToFlash += Time.deltaTime;
            if (_curTimeToFlash >= _timeToFlash)
            {
                _curTimeToFlash = 0.0f;
                GameBoardPrefab.transform.gameObject.SetActive(!GameBoardPrefab.transform.gameObject.activeSelf);
            }

            _curTimeToDestroyFlyObject += Time.deltaTime;
            if (_curTimeToDestroyFlyObject >= _timeToDestroyFlyObject)
            {
                _curTimeToDestroyFlyObject = 0.0f;
                foreach (var o in _deleteObejct)
                {
                    _deleteObejct.Remove(o);
                    Destroy(o);
                    break;
                }
            }
        }

        private void DescretingOneLive()
        {
            _isDestroyPause = true;
            _curTimeToPause = 0.0f;
            _curTimeToFlash = 0.0f;
            _curTimeToDestroyFlyObject = 0.0f;

            _deleteObejct.Clear();

            var gos = Root.GetComponentsInChildren<GameFlyObject>();

            foreach (var gameFlyObject in gos)
            {
                gameFlyObject.SetPause();

                _deleteObejct.Add(gameFlyObject.gameObject);
            }

            if (SoundsManager.Instance != null)
                SoundsManager.Instance.AddSound(gameObject, "CATCH_RUBUSH");

            // удаляем звук двежения
            if (SoundsManager.Instance != null)
                SoundsManager.Instance.DelSound(gameObject, "MOVIE_BOARD");
        }

        void UpdateTime()
        {
            if(_isDestroyPause)
                return;

            _CurentTimeRespawn += Time.deltaTime;

            if (_CurentTimeRespawn >= _TimeRespawn)
            {
                _CurentTimeRespawn = 0;
                UpdateRespawn();
            }
        }

        void UpdateRespawn()
        {
            var go = GameObject.Instantiate(GameFlyObjectPrefab);
            go.GetComponent<RectTransform>().SetParent(Root.transform);
            go.GetComponent<RectTransform>().localScale = new Vector3(1,1,1);

            var typeObject = Random.Range(0, 7);
            var offset = 100.0f;
            var position = Random.Range(offset, _sizeCanvas.x - offset);

            go.Init((EnumTypeFlyObject)typeObject, new Vector3(position, 200, 0), _speedObject);
        }

        private void OnActionColision(GameObject go)
        {
            var flyObject = go.GetComponent<GameFlyObject>();

            if (flyObject.GetTypeFlyObject() == EnumTypeFlyObject.Telephone )
            {
                _scores+= 10;
                GameBoardPrefab.SetScoresText(_scores);

                Debug.Log("play sound");

                if (SoundsManager.Instance != null)
                    SoundsManager.Instance.AddSound(gameObject, "CATCH_BONUS");
            }
            else
            {
                Debug.Log("Dicrement one live ");
                GameLive.DecrementLive();
                Handheld.Vibrate();
                DescretingOneLive();
            }
            
            Destroy(go);
        }

        void UpdateInput()
        {
            _sizeBoard = GameBoardPrefab.GetSizeGameBoard();
            _sizeCanvas = CanvasGame.GetComponent<RectTransform>().sizeDelta;
            _scaleCanvas = CanvasGame.GetComponent<RectTransform>().localScale;
            _halfSizeBoard = new Vector3(_sizeBoard.x / 2.0f, 0, 0);

            EndLineController.GetComponent<RectTransform>().localPosition = new Vector3(0, -_sizeCanvas.y, 0);

            if (_isDestroyPause)
                return;

#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS)
            /*if (Input.touchCount > 0)
            {
                foreach (var touch in Input.touches)
                {
                    if (touch.phase == TouchPhase.Moved)  
                    {
                        _NowPosition = touch.position;
                    }
                }
            }
            else*/
            {
                _NowPosition.x += Input.acceleration.x* _speedBoard;
                var np = ValidationPosition(_NowPosition);
                _NowPosition = new Vector3(np.x * _scaleCanvas.x, np.y * _sizeCanvas.y, 0);
            }
#else

            if (Input.GetMouseButton(0))
            {
                _NowPosition = Input.mousePosition;
            }
#endif
            var p = ValidationPosition(_NowPosition);
            p.y = -(_sizeCanvas.y - _sizeBoard.y - 50);

            GameBoardPrefab.GetComponent<RectTransform>().localPosition = p - _halfSizeBoard;
        }


        Vector3 ValidationPosition(Vector3 pos)
        {
            var p = new Vector3(pos.x / _scaleCanvas.x, pos.y / _scaleCanvas.y, 0);
            p -= _halfSizeBoard;
            p.x = p.x < 0 ? 0 : p.x > (_sizeCanvas.x - _sizeBoard.x) ? _sizeCanvas.x - _sizeBoard.x : p.x;
            p += _halfSizeBoard;

            return p;
        }

        void OnCollisionEnter2D(Collision2D coll)
        {
            //Debug.Log("coolision");
            //Debug.Log("coolision");

            if (coll.gameObject.tag == "GameBoard")
            {
                
            }


        }
    }

}