﻿using System;
using UnityEngine;
using System.Collections;

namespace ArcanoidGame
{
    
    public class GameFlyObject : MonoBehaviour
    {
        public UnityEngine.UI.Image[] Images;

        private UnityEngine.UI.Image GameImage;

        private EnumTypeFlyObject _TypeFlyObject;

        private float _timeToDelete = 0;

        private float _speed = 0;

        private bool _isPause = false;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (_isPause)
                return;

            var pos = GetComponent<RectTransform>().localPosition;
            pos.y -= Time.deltaTime*_speed;
            GetComponent<RectTransform>().localPosition = pos;

            _timeToDelete += Time.deltaTime;
            if (_timeToDelete > 1.0f)
            {
                _timeToDelete = 0;
                if (GetComponent<RectTransform>().localPosition.y < -4000)
                {
                    Destroy(gameObject);
                }
            }
        }

        public void SetPause(bool pause = true)
        {
            _isPause = pause;
        }

        public EnumTypeFlyObject GetTypeFlyObject()
        {
            return _TypeFlyObject;
        }

        public void Init(EnumTypeFlyObject type, Vector3 pos, float speed)
        {
            _speed = speed;
            _TypeFlyObject = type;

            GameImage = Images[(int) type];
            GameImage.gameObject.SetActive(true);
            
            /*
            

            if (type == EnumTypeFlyObject.Telephone)
                GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().sizeDelta.x / 8.0f, GetComponent<RectTransform>().sizeDelta.y / 8.0f);

            GameImage.GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
            GameBoxCollider2D.size = GameImage.GetComponent<RectTransform>().sizeDelta;
            

            if(type == EnumTypeFlyObject.Hamer)
                GetComponent<RectTransform>().localEulerAngles = new Vector3(0,0,127);
            else if(type == EnumTypeFlyObject.Brick)
                GetComponent<RectTransform>().localEulerAngles = new Vector3(0, 0, -90);
            else
                GetComponent<RectTransform>().localEulerAngles = new Vector3(0, 0, 0);
                */


            GetComponent<RectTransform>().localPosition = pos;
        }
    }
}