﻿using UnityEngine;
using System.Collections;
using UI.Game;
using UnityEngine.SceneManagement;

namespace MainMenuMySuperGame
{
    public class MainMenu : MonoBehaviour
    {
        public ButtonController ShooterGameButton;
        public ButtonController ArcanoidGameButton;
        public ButtonController HidenObjectGameButton;
        public ButtonController SwimButtonController;

        // Use this for initialization
        void Start()
        {
            ShooterGameButton.OnActionClick = delegate (GameObject o)
            {
                SceneManager.LoadScene("Scenes/ShooterGame/GameScene", LoadSceneMode.Single);
            };

            ArcanoidGameButton.OnActionClick = delegate (GameObject o)
            {
                SceneManager.LoadScene("Scenes/ArkanoidGame/GameScene", LoadSceneMode.Single);
            };

            HidenObjectGameButton.OnActionClick = delegate (GameObject o)
            {
                SceneManager.LoadScene("Scenes/HidenGame/GameScene", LoadSceneMode.Single);
            };

            SwimButtonController.OnActionClick = delegate (GameObject o)
            {
                SceneManager.LoadScene("Scenes/SwimerGame/GameScene", LoadSceneMode.Single);
            };
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}