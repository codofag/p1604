﻿using UnityEngine;
using System.Collections;

public class Text3D : MonoBehaviour
{
    public TextMesh TextMesh;
    
    private float _speed = 1.0f;
    private float _alpha;
    private bool _isDelete = false;
    private GameObject _cameraTransform;
    private Color _color;

	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	    if (_isDelete)
	        return;

        var t = GetComponent<Transform>();
        var position = t.position + (t.up * Time.deltaTime * _speed);
        t.position = position;
        
	    _alpha -= Time.deltaTime / 2.0f;
	    if (_alpha <= 0)
	    {
	        _isDelete = true;
	        Destroy(gameObject);
	        return;
	    }

	    SetColor();

        GetComponent<Transform>().LookAt(_cameraTransform.transform.forward);
	}

    public void Init(Vector3 position, string text, GameObject camera, Color color)
    {
        GetComponent<Transform>().localPosition = position;

        TextMesh.text = text;

        _alpha = 1.0f;
        _color = color;
        SetColor();

        _cameraTransform = camera;

        GetComponent<Transform>().LookAt(_cameraTransform.transform.forward);
    }

    private void SetColor()
    {
        _color.a = _alpha;
        TextMesh.color = _color;
    }
}
