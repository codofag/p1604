﻿using UnityEngine;
using System.Collections;

namespace ShootGame
{

    public class SimpleCamera : MonoBehaviour
    {
        private WebCamTexture _webcamTexture = null;

        public UnityEngine.UI.RawImage CameraImage;
        public UnityEngine.UI.Text Text;
        public Camera MainCamera;

        public Canvas MainCanvas;

        // Use this for initialization
        void Start()
        {
            var devices = WebCamTexture.devices;

            string nameDevice = "";
            foreach (var webCamDevice in devices)
            {
                if (!webCamDevice.isFrontFacing)
                    nameDevice = webCamDevice.name;

            }

            _webcamTexture = new WebCamTexture(nameDevice);//, (int)(Screen.height/2.0f), (int)(Screen.width / 2.0f));
            CameraImage.texture = _webcamTexture;

            ShowCamera(true);

            float scaleY = _webcamTexture.videoVerticallyMirrored ? -1f : 1f;
            CameraImage.GetComponent<RectTransform>().localScale = new Vector3(1f, scaleY, 1f);
        }

        void OnDestroy()
        {
            //ShowCamera(false);
           // Destroy(_webcamTexture);
        }

        public void ShowCamera(bool show)
        {
            if (_webcamTexture.deviceName == "")
                return;

            if (show)
                _webcamTexture.Play();
            else
                _webcamTexture.Stop();
        }

        // Update is called once per frame
        void Update()
        {
            RectTransform rectTransform = MainCanvas.GetComponent<RectTransform>();
            //CameraImage.GetComponent<RectTransform>().localEulerAngles = new Vector3(0, 90, 0);
            if (_webcamTexture.videoRotationAngle == 90 || _webcamTexture.videoRotationAngle == 270)
            {
                CameraImage.GetComponent<RectTransform>().localEulerAngles = new Vector3(0, 0,
                    -_webcamTexture.videoRotationAngle);
                CameraImage.GetComponent<RectTransform>().sizeDelta = new Vector2(rectTransform.sizeDelta.y,
                    rectTransform.sizeDelta.x);
            }
            else
            {
                CameraImage.GetComponent<RectTransform>().localEulerAngles = new Vector3(0, 0,
                    _webcamTexture.videoRotationAngle);
                CameraImage.GetComponent<RectTransform>().sizeDelta = new Vector2(rectTransform.sizeDelta.x,
                    rectTransform.sizeDelta.y);
            }

            Text.text = "cord := " + CameraImage.GetComponent<RectTransform>().localEulerAngles + " camera angle := " +
                        MainCamera.transform.eulerAngles;
        }


    }
}