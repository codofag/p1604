﻿

namespace ShootGame
{
    public enum TypeDevils
    {
        Sad,
        Happy,
        Angry
    }

    public enum LifeCycleDevils
    {
        Rebirth,
        Life,
        Death,
        Corpse,
    }
}