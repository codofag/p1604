﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Game.UI;
using ShootGame;
using SoundManager;
using UI.Game;
using Random = UnityEngine.Random;

namespace ShootGame
{

    public class GameLoop : MonoBehaviour
    {
        public Camera Target;

        public GameObject RootDevils;
        public Devils GameObjectPrefabDevils;

        public Text3D Text3DController;

        private int _maxNumberWave = 5;
        private int _currentNumberWave = 0;

        private int _devilInGroup = 5;

        private float _GameTimeToRespawn = 10.0f;
        private float _CurrentGameTimeToRespawn = 8.0f;

        private float _GameTimeToIncrimentWave = 60.0f;
        private float _CurrentGameTimeToIncrimentWave = 0.0f;

        private float _MaxSpeedDevils = 2.0f;
        private float _IncrimentSpeedDevils = 0.2f / 1.0f;  
        private float _CurrentSpeedDevils = 1.0f / 3.0f;

        private float _MinDistanceSpawnDevils = 12.0f;
        private float _MaxDistanceSpawnDevils = 16.0f;

        private int _MaxTypeDevils = 3;

        private int _DevilsDie = 0;
        private bool _StopGame = false;

        private List<Devils> _devils = new List<Devils>();
        private bool _isPlayAlarmSound = false;

        private int _hitForClick = 10;
        private int _hitForDeath = 15;

        // Use this for initialization
        void Start()
        {
            Screen.orientation = ScreenOrientation.Portrait;

            if (SoundsManager.Instance != null)
                SoundsManager.Instance.AddSound(gameObject, "START_GAME");

            if (UIManager.Instance != null)
            {
                UIManager.Instance.OnActionPauseGame = OnActionPauseGame;
                UIManager.Instance.CreateUI();
                UIManager.Instance.CreateTypeUI(EnumUIType.BottomSetingButton);
                UIManager.Instance.CreateTypeUI(EnumUIType.Radar);
            }
        }

        private void OnActionPauseGame(bool pauseGame)
        {
            _StopGame = pauseGame;

            foreach (var devilse in _devils)
            {
                devilse.SetPause(pauseGame);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (_StopGame)
                return;

            UpdateTime();
            UpdateLifeDevils();
            UpdateInput();

            if(UIManager.Instance != null)
                UIManager.Instance.SetUIValue(EnumUIType.Radar, Target.gameObject, _devils);
            //Radar.UpdatePosition(Target.gameObject, _devils);
        }

        private void UpdateTime()
        {
            _CurrentGameTimeToIncrimentWave += Time.deltaTime;
            _CurrentGameTimeToRespawn += Time.deltaTime;

            // once 20 sec
            if (_CurrentGameTimeToRespawn >= _GameTimeToRespawn)
            {
                RespawnDevils();
                _CurrentGameTimeToRespawn = 0;
            }

            // once 60 sec
            if (_CurrentGameTimeToIncrimentWave >= _GameTimeToIncrimentWave)
            {
                _currentNumberWave++;
                _devilInGroup++;

                _CurrentGameTimeToIncrimentWave = 0;

                _CurrentSpeedDevils += _IncrimentSpeedDevils;
                if (_CurrentSpeedDevils > _MaxSpeedDevils)
                    _CurrentSpeedDevils = _MaxSpeedDevils;
            }
        }

        private void UpdateLifeDevils()
        {
            var length = 1000.0f;
            foreach (var devilse in _devils)
            {
                if (devilse.IsDeath())
                {
                    _devils.Remove(devilse);
                    GameObject.Destroy(devilse.gameObject);
                    break;
                }

                var l = Vector3.Distance(devilse.GetComponent<Transform>().localPosition, Vector3.zero);
                if (l < length)
                    length = l;
            }

            //Debug.Log("length wait := " + length);

            if (length < 5 && !_isPlayAlarmSound)
            {
                _isPlayAlarmSound = true;
                if (SoundsManager.Instance != null)
                    SoundsManager.Instance.AddSound(gameObject, "DEVIL_NEAR");

                //Debug.Log("length start := " + length);
            }
            if (length > 5 && _isPlayAlarmSound)
            {
                //Debug.Log("length end := " + length);

                if (SoundsManager.Instance != null)
                    SoundsManager.Instance.DelSound(gameObject, "DEVIL_NEAR");
            }
        }

        private void RespawnDevils()
        {
            if (SoundsManager.Instance != null)
                SoundsManager.Instance.AddSound(gameObject, "DEVIL_RESPAWN");

            for (int i = 0; i < _devilInGroup; i++)
            {
                CreateNewDevil();
            }
        }

        private void CreateNewDevil()
        {
            //var angleCamera = 90 - Target.GetComponent<Transform>().eulerAngles.y;

            //Debug.Log("angle = " + angleCamera);
            //var RadiusOfSpawn = 20;

            //var angle = Random.Range(70, 110.0f);
            var angle = Random.Range(0.0f, 359.0f);
            var r = Random.Range(_MinDistanceSpawnDevils, _MaxDistanceSpawnDevils);
            var typeDevil = Random.Range(0, _MaxTypeDevils);
            var speed = _CurrentSpeedDevils;

            var y = r*Mathf.Sin(Mathf.Deg2Rad*angle);
            var x = r*Mathf.Cos(Mathf.Deg2Rad*angle);

            var targetPosition = new Vector3(0,0.5f,0);
            var points = GenerationPoint(new Vector3(x, 0, y), targetPosition);

            var go = GameObject.Instantiate(GameObjectPrefabDevils);
            go.GetComponent<Transform>().SetParent(RootDevils.transform);
            go.Init(typeDevil, speed, new Vector3(x, 0, y), points);
            go.OnActionDevilsWin = OnActionDevilWin;
            //go.SetPoints(points);

            _devils.Add(go);
        }

        private void OnActionDevilWin(GameObject go)
        {
            foreach (var devilse in _devils)
            {
                devilse.SetLifeCycleDevils(LifeCycleDevils.Corpse);
            }

            _StopGame = true;

            if (SoundsManager.Instance != null)
                SoundsManager.Instance.AddSound(gameObject, "FAIL_GAME");

            if (UIManager.Instance != null)
            {
                UIManager.Instance.CreateTypeUI(EnumUIType.EndGameMenu);
                UIManager.Instance.SetUIValue(EnumUIType.EndGameMenu, _DevilsDie);
            }
        }

        private List<Vector3> GenerationPoint(Vector3 pointStart, Vector3 pointEnd)
        {
            List<Vector3> p = new List<Vector3>();

            p.Add(pointStart);

            float r = 3;

            for (int i = 0; i < _maxNumberWave - _currentNumberWave; i++)
            {
                var pos = Vector3.Lerp(pointStart, pointEnd, (1.0f/(_maxNumberWave - _currentNumberWave))*i);
                var z = Random.Range(pos.z - r, pos.z + r);
                var x = Random.Range(pos.x - r, pos.x + r);

                //Debug.Log(" x := " + x + " z := " + z);

                p.Add(new Vector3(x, 0, z));
            }

            p.Add(pointEnd);

            return p;
        }

        private void Create3DText(Vector3 position, string text, Color32 color)
        {
            position+= new Vector3(0,0,0);

            var go = Instantiate(Text3DController);
            go.Init(position, text, Target.gameObject, color);
            go.GetComponent<Transform>().SetParent(RootDevils.transform);
        }

        public void UpdateInput()
        {
#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS)
        foreach (var touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                CastHit(touch.position);
            }
        }
#else

            if (Input.GetMouseButtonDown(0))
            {
                //Debug.Log("mouse button Down");
                CastHit(Input.mousePosition);
            }
#endif
        }

        public void CastHit(Vector2 position)
        {
            var ray = Camera.main.ScreenPointToRay(position);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 1000))
            {
                if (hit.collider.gameObject.tag == "Devils")
                {
                    var di = hit.collider.gameObject.GetComponent<Devils>();
                    di.LifeToDeath();
                    var cdi = di.GetLifeCycleDevil();
                    var colorGreen = Color.green;
                    var colorRed = Color.red;
                    if (cdi != LifeCycleDevils.Corpse)
                    {
                        if (cdi != LifeCycleDevils.Death)
                        {
                            _DevilsDie += 10;
                            Create3DText(di.GetComponent<Transform>().localPosition, "+" + _hitForClick, colorGreen);
                        }
                        else
                        {
                            _DevilsDie += 15;
                            Create3DText(di.GetComponent<Transform>().localPosition, "+" + _hitForDeath, colorRed);
                        }
                    }

#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS)
                    //Handheld.Vibrate();
#endif

                }
            }
        }
    }
}