﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SoundManager;

namespace ShootGame
{

    public class Devils : MonoBehaviour
    {
        public GameObject[] TypeDevils;

        public Action<GameObject> OnActionDevilsWin;

        private GameObject _devil;
        private List<Material> _materials = new List<Material>();

        private float _speed;
        private LifeCycleDevils _lifeCycleDevils = LifeCycleDevils.Corpse;
        private float _disolve;

        private int _typeDevils = 0;

        private bool _isDeath = false;

        private List<Vector3> _points = new List<Vector3>();
        private int _indexPoints = 0;

        //private Vector3 _firstPoint;
        private Vector3 _seconPoint;

        private bool _pause = false;
        
        // Use this for initialization
        void Start()
        {

        }

        public void Init(int type, float speed, Vector3 pos, List<Vector3> points)
        {
            _typeDevils = type;

            _speed = speed;

            var trans = GetComponent<Transform>();
            trans.localPosition = pos;
            trans.LookAt(Vector3.zero);

            SwitchDevils(type);

            SetPoints(points);

            RebirthDevil();
        }

        void GetAllMaterials()
        {
            _materials.Clear();

            var renders = _devil.GetComponentsInChildren<Renderer>();
            foreach (var render in renders)
            {
                _materials.Add(render.material);
            }
        }

        void SetDisolveToMaterial(float dis)
        {
            foreach (var material in _materials)
            {
                material.SetFloat("_DissolvePercentage", dis);
            }
        }

        public void RebirthDevil()
        {
            _lifeCycleDevils = LifeCycleDevils.Rebirth;
            _disolve = 1.0f;

            SetDisolveToMaterial(_disolve);
        }

        private void SwitchDevils(int type)
        {
            foreach (var typeDevil in TypeDevils)
            {
                typeDevil.SetActive(false);
            }

            _devil = TypeDevils[type];
            _devil.SetActive(true);

            GetAllMaterials();
        }

        public LifeCycleDevils GetLifeCycleDevil()
        {
            return _lifeCycleDevils;
        }

        public void LifeToDeath()
        {
            if (_lifeCycleDevils != LifeCycleDevils.Life)
                return;

            _typeDevils--;
            if (_typeDevils >= 0)
            {
                if (_typeDevils == 1)
                {
                    if (SoundsManager.Instance != null)
                        SoundsManager.Instance.AddSound(gameObject, "CLICK_TO_DEVIL2");
                }
                else
                {
                    if (SoundsManager.Instance != null)
                        SoundsManager.Instance.AddSound(gameObject, "CLICK_TO_DEVIL");
                }

                SwitchDevils(_typeDevils);
                return;
            }

            if (SoundsManager.Instance != null)
                SoundsManager.Instance.AddSound(gameObject, "DEVIL_DEAD");

            GetComponent<SphereCollider>().enabled = false;
            _disolve = 0.0f;

            _lifeCycleDevils = LifeCycleDevils.Death;
            GetComponent<Transform>().eulerAngles = new Vector3(-30, GetComponent<Transform>().eulerAngles.y, 0);
        }

        private void UpdateLifeDevil()
        {
            if (_lifeCycleDevils == LifeCycleDevils.Rebirth)
            {
                _disolve -= Time.deltaTime;
                if (_disolve <= 0.0f)
                {
                    _disolve = 0.0f;
                    _lifeCycleDevils = LifeCycleDevils.Life;
                }

                SetDisolveToMaterial(_disolve);
            }
            else if (_lifeCycleDevils == LifeCycleDevils.Life)
            {
                MovePoint();
            }
            else if (_lifeCycleDevils == LifeCycleDevils.Death)
            {
                _disolve += Time.deltaTime;

                if (_disolve > 1.0f)
                {
                    _disolve = 1.0f;
                    _lifeCycleDevils = LifeCycleDevils.Corpse;
                    _isDeath = true;
                }

                SetDisolveToMaterial(_disolve);
            }
        }

        private void MovePoint()
        {
            GetComponent<Transform>().LookAt(_seconPoint);

            var t = GetComponent<Transform>();
            var position = t.position + (t.forward*Time.deltaTime*_speed);
            t.position = position;

            float distance = 0.2f;
            if (_indexPoints + 1 >= _points.Count)
                distance = 0.5f;

            if (Vector3.Distance(position, _seconPoint) < distance)
            {
                SetNextPoint();
            }
        }

        public void SetLifeCycleDevils(LifeCycleDevils l)
        {
            _lifeCycleDevils = l;
        }

        private void SetPoints(List<Vector3> points)
        {
            _points = new List<Vector3>();
            _points.AddRange(points);

            if (_points.Count < 1)
            {
                _isDeath = true;
                return;
            }

            SetNextPoint();
        }

        private void SetNextPoint()
        {
            if (_indexPoints == 0)
            {
                _seconPoint = _points[1];

                _indexPoints = 1;
                return;
            }

            _indexPoints++;

            if (_points.Count <= _indexPoints)
            {
                if (OnActionDevilsWin != null)
                    OnActionDevilsWin(gameObject);

                _lifeCycleDevils = LifeCycleDevils.Corpse;
                _devil.SetActive(false);
                return;
            }

            _seconPoint = _points[_indexPoints];
        }

        // Update is called once per frame
        void Update()
        {
            if (_pause)
                return;

            UpdateLifeDevil();

            for (int i = 1; i < _points.Count; i++)
            {
                Debug.DrawLine(_points[i - 1], _points[i]);
            }
        }

        public bool IsDeath()
        {
            return _isDeath;
        }

        public void SetPause(bool pause)
        {
            _pause = pause;
        }
    }

}