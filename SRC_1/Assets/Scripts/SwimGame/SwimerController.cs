﻿using System;
using UnityEngine;
using System.Collections;
using SwimGame;


public class SwimerController : MonoBehaviour
{
    public GameObject RootPosition;
    public GameObject BodyRotation;
    public GameObject PhysicBody;

    public AnimationManagerSwim AnimationManagerSwimController;

    public GameObject Swimer;

    private float _speed = 300.0f;
    private bool _pause = true;

    private Vector3 _positionStand = new Vector3(585, -415);
    private Vector3 _positionStair = new Vector3(735, -410);
    private Vector3 _positionJump = new Vector3(880, -430);
    private Vector3 _savePosition = new Vector3(500,-500);

    // Use this for initialization
    void Start ()
    {
        Screen.orientation = ScreenOrientation.Landscape;

        SetAnimationSwim(new Vector3(500,-500));
    }
	
	// Update is called once per frame
	void Update ()
	{
	    //if (_pause)
	     //   return;

	    //UpdateInput();
	}

    void UpdateInput()
    {
        Vector3 InputPosition = Vector3.zero;
#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS)
        if (Input.touchCount > 0)
        {
            foreach (var touch in Input.touches)
            {
                if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
                {
                    InputPosition = touch.position;
                }
            }
        }
#else

        if (Input.GetMouseButton(0))
        {
            InputPosition = Input.mousePosition;
        }
#endif

        if (InputPosition == Vector3.zero)
        {
            RootPosition.GetComponent<Rigidbody2D>().velocity = RootPosition.GetComponent<Rigidbody2D>().velocity.normalized;
            return;
        }


        var t = RootPosition.GetComponent<RectTransform>().transform;
        var dir = InputPosition - t.position;
        float AngleRad = Mathf.Atan2(dir.y, dir.x);
        float AngleDeg = (180 / Mathf.PI) * AngleRad;
        var angle = Quaternion.Euler(0, 0, AngleDeg).eulerAngles;

        BodyRotation.GetComponent<RectTransform>().eulerAngles = angle;
        PhysicBody.GetComponent<RectTransform>().eulerAngles = angle;

        if (angle.z > 90 && angle.z < 270)
        {
            BodyRotation.GetComponent<RectTransform>().localScale = new Vector3(1, -1, 1);
            PhysicBody.GetComponent<RectTransform>().localScale = new Vector3(1, -1, 1);
        }
        else
        {
            BodyRotation.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            PhysicBody.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        }

        var p = RootPosition.GetComponent<Rigidbody2D>();

        var v = dir.normalized * _speed;

        if (Vector3.Distance(dir, Vector3.zero) <= 20)
            v = Vector3.zero;

        p.velocity = v;
    }

    public void PhysicMovie(Vector3 InputPosition)
    {
        if (InputPosition == Vector3.zero)
        {
            RootPosition.GetComponent<Rigidbody2D>().velocity = RootPosition.GetComponent<Rigidbody2D>().velocity.normalized;
            return;
        }

        var t = RootPosition.GetComponent<RectTransform>().transform;
        var dir = InputPosition - t.position;
        float AngleRad = Mathf.Atan2(dir.y, dir.x);
        float AngleDeg = (180 / Mathf.PI) * AngleRad;
        var angle = Quaternion.Euler(0, 0, AngleDeg).eulerAngles;

        BodyRotation.GetComponent<RectTransform>().eulerAngles = angle;
        PhysicBody.GetComponent<RectTransform>().eulerAngles = angle;

        if (angle.z > 90 && angle.z < 270)
        {
            BodyRotation.GetComponent<RectTransform>().localScale = new Vector3(1, -1, 1);
            PhysicBody.GetComponent<RectTransform>().localScale = new Vector3(1, -1, 1);
        }
        else
        {
            BodyRotation.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            PhysicBody.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        }

        var p = RootPosition.GetComponent<Rigidbody2D>();

        var v = dir.normalized * _speed;

        if (Vector3.Distance(dir, Vector3.zero) <= 20)
            v = Vector3.zero;

        p.velocity = v;
    }

    public Vector3 GetPosition()
    {
        return RootPosition.GetComponent<RectTransform>().localPosition;
    }

    public void SetPause(bool pause)
    {
        _pause = pause;

        if (pause)
        {
            RootPosition.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        }
        else
        {
            RootPosition.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None | RigidbodyConstraints2D.FreezeRotation;
        }
    }

    public void SetAnimationStand()
    {
        AnimationManagerSwimController.State = AnimationManagerSwim.StateAnimation.Stand;
        RootPosition.GetComponent<RectTransform>().localPosition = _positionStand;
        Freeze(true);
    }

    public void SetAnimationStair()
    {
        AnimationManagerSwimController.State = AnimationManagerSwim.StateAnimation.Stair;
        RootPosition.GetComponent<RectTransform>().localPosition = _positionStair;
        NormalaizeScale();
        Freeze(true);
    }

    public void SetAnimationJump()
    {
        AnimationManagerSwimController.State = AnimationManagerSwim.StateAnimation.Jump;
        RootPosition.GetComponent<RectTransform>().localPosition = _positionJump;
        Freeze(true);
    }

    public void SetAnimationSwim(Vector3 position)
    {
        AnimationManagerSwimController.State = AnimationManagerSwim.StateAnimation.Swim;
        RootPosition.GetComponent<RectTransform>().localPosition = position;
        Freeze(false);
    }

    public void Freeze(bool freeze)
    {
        RootPosition.GetComponent<Rigidbody2D>().constraints = freeze ? RigidbodyConstraints2D.FreezeAll : RigidbodyConstraints2D.None | RigidbodyConstraints2D.FreezeRotation;
    }

    public void NormalaizeScale()
    {
        BodyRotation.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        PhysicBody.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

        BodyRotation.GetComponent<RectTransform>().eulerAngles = Vector3.zero;
        PhysicBody.GetComponent<RectTransform>().eulerAngles = Vector3.zero;
    }
}
