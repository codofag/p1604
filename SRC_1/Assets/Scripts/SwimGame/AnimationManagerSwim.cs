﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using HidenGame;
using SoundManager;
using XMLReader;

namespace SwimGame
{

    public class AnimationManagerSwim : MonoBehaviour
    {
        public enum StateAnimation
        {
            Swim,
            Jump,
            Calm,
            Stand,
            Stair
        }

        public AnimationController AnimationController;

        public Action<GameObject> OnActionTrigerEnter;

        List<List<Sprite>> _spritesAnimation;

        private List<NameAnimationXml> _animationXmls = new List<NameAnimationXml>();

        private bool _isWakeUp = false;

        public StateAnimation State = StateAnimation.Jump;
        private int _stateAnimation = (int) StateAnimation.Jump;


        // Use this for initialization
        void Start()
        {
            _spritesAnimation = new List<List<Sprite>>();
            _spritesAnimation.Add(new List<Sprite>(Resources.LoadAll<Sprite>("Sprites/SwimerGame/Swimer/Swim/ASwim")));
            _spritesAnimation.Add(new List<Sprite>(Resources.LoadAll<Sprite>("Sprites/SwimerGame/Swimer/Jump/AJump")));
            _spritesAnimation.Add(new List<Sprite>(Resources.LoadAll<Sprite>("Sprites/SwimerGame/Swimer/Calm/ACalm")));
            _spritesAnimation.Add(new List<Sprite>(Resources.LoadAll<Sprite>("Sprites/SwimerGame/Swimer/Stand/AStand")));
            _spritesAnimation.Add(new List<Sprite>(Resources.LoadAll<Sprite>("Sprites/SwimerGame/Swimer/Stair/AStair")));

            _animationXmls.Clear();
            _animationXmls = AnimationXML.GetAnimation(AnimationXML.FileAnimationSwimer);

            AnimationController.Init(_animationXmls[_stateAnimation].GetFrames(),
                _spritesAnimation[_stateAnimation].ToArray());
            AnimationController.Play();
            //AnimationController.OnActionEndAnimation = OnActionEndAnimation;
        }

        // Update is called once per frame
        void Update()
        {
            if (State != (StateAnimation) _stateAnimation)
            {
                _stateAnimation = (int) State;
                SetNewStateAnimation();
            }
        }

        private void OnActionEndAnimation()
        {
            /*
            if (!_isWakeUp)
            {
                if (SoundsManager.Instance != null)
                    SoundsManager.Instance.AddSound(gameObject, "HIDEN_HRAP");
                return;
            }

            _stateAnimation++;
            if (_stateAnimation > (int)StateAnimation.Run2)
            {
                _stateAnimation = (int)StateAnimation.Run2;

                if (OnActionEndFullAnimation != null)
                    OnActionEndFullAnimation();

                return;
            }

            if (_stateAnimation == (int)StateAnimation.Walk)
            {
                if (SoundsManager.Instance != null)
                    SoundsManager.Instance.AddSound(gameObject, "HIDEN_WALK");
            }
            else if (_stateAnimation == (int)StateAnimation.Run1)
            {
                if (SoundsManager.Instance != null)
                    SoundsManager.Instance.AddSound(gameObject, "HIDEN_RUN");
            }
            else if (_stateAnimation == (int)StateAnimation.Awake)
            {
                if (SoundsManager.Instance != null)
                    SoundsManager.Instance.DelSound(gameObject, "HIDEN_HRAP");
            }

            AnimationController.Init(_animationXmls[_stateAnimation].GetFrames(),
                _spritesAnimation[_stateAnimation].ToArray());
            AnimationController.Play();*/
        }

        public void WakeUpMisterAnderson()
        {
            _isWakeUp = true;
            OnActionEndAnimation();
        }

        public void SetNewStateAnimation()
        {
            AnimationController.Init(_animationXmls[_stateAnimation].GetFrames(),
                _spritesAnimation[_stateAnimation].ToArray());
            AnimationController.Play();
            AnimationController.SetOffset();
        }

        void OnCollisionEnter2D(Collision2D coll)
        {
            foreach (var contactPoint2D in coll.contacts)
            {
                Debug.Log("colider := " + contactPoint2D.collider.name);
                
            }
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if (OnActionTrigerEnter != null)
                OnActionTrigerEnter(other.gameObject);
        }
    }
}