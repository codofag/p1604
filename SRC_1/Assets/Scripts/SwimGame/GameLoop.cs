﻿using System;
using UnityEngine;
using System.Collections;
using Game.UI;


namespace SwimGame
{
    public class GameLoop : MonoBehaviour
    {
        enum EnumStateGame
        {
            Start,
            Jump,
            Swim,
            Mirmind,
            Shark,
            Stair,
        }

        public Canvas GameCanvas;
        public SwimerController GameSwimController;
        public GameObject SwimerSide;
        public GameObject Level;

        private Vector3 BackgroundSize = new Vector3(9600, 6480);
        private Vector3 _canvasSize = Vector3.zero;

        private Vector3 _levelVelocity = Vector3.zero;
        

        private Vector3 _finishPositionCamera = Vector3.zero;
        private Vector3 _startPositionCamera = Vector3.zero;

        private float _curTimeUpdateCamera = 0;
        private float _timeUpdateCamera = 1.6f;
        private bool _isMovieCamera = false;
        private bool _IsMovieScene = true;

        private bool _pause = false;
        private EnumStateGame _enumStateGame = EnumStateGame.Start;
        


        // Use this for initialization
        void Start()
        {
            if (UIManager.Instance != null)
            {
                UIManager.Instance.OnActionPauseGame = OnActionPauseGame;
                UIManager.Instance.CreateUI();
                UIManager.Instance.CreateTypeUI(EnumUIType.BottomSetingButton);
            }

            GameSwimController.SetAnimationStand();
            GameSwimController.AnimationManagerSwimController.AnimationController.OnActionEndAnimation = OnActionEndAnimationSwimer;
            GameSwimController.AnimationManagerSwimController.OnActionTrigerEnter = OnActionTrigerEnterSwimer;
            _enumStateGame = EnumStateGame.Start;
        }

        private void OnActionTrigerEnterSwimer(GameObject gameObject)
        {
            if (_enumStateGame == EnumStateGame.Swim)
            {
                _enumStateGame = EnumStateGame.Stair;
                GameSwimController.SetAnimationStair();
                SetupCamera(Level.GetComponent<RectTransform>().localPosition, Vector3.zero, 9);
                _IsMovieScene = false;
            } 
        }

        private void OnActionPauseGame(bool pause)
        {
            _pause = pause;
            GameSwimController.SetPause(pause);
        }

        private void OnActionEndAnimationSwimer()
        {
            Debug.Log("swim := " + _enumStateGame + " _enumStateGame := " + _enumStateGame + " _isPause := " + _pause + " _isMovieCamera := " + _isMovieCamera);
             
            if (_enumStateGame == EnumStateGame.Jump)
            {
                _enumStateGame = EnumStateGame.Swim;
                GameSwimController.SetAnimationSwim(new Vector3(1900,-1079));
                
                //_positionMovieCamera = -GameSwimController.GetPosition();
                GameSwimController.Freeze(true);
                SetupCamera(Level.GetComponent<RectTransform>().localPosition , - GameSwimController.GetPosition(), 2);
                //1184 + 880, -749 + 430
            }
            else if (_enumStateGame == EnumStateGame.Stair)
            {
                _enumStateGame = EnumStateGame.Start;
                GameSwimController.SetAnimationStand();
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (_pause)
                return;

            if (_isMovieCamera)
                UpdateMovieCamera();
            else if(_IsMovieScene)
            {
                UpdateInput();
                UpdateMoveObjects();
            }
        }

        private void UpdateInput()
        {
            Vector3 InputPosition = Vector3.zero;

            _canvasSize = GameCanvas.GetComponent<RectTransform>().sizeDelta;

#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS)
        if (Input.touchCount > 0)
        {
            foreach (var touch in Input.touches)
            {
                if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
                {
                    InputPosition = touch.position;
                }
            }
        }
#else

            if (Input.GetMouseButton(0))
            {
                InputPosition = Input.mousePosition;
            }
#endif

            // если мы нажали на кнопку то прыгаем!
            if (_enumStateGame == EnumStateGame.Start && InputPosition != Vector3.zero)
            {
                Debug.Log("start Anim");

                _enumStateGame = EnumStateGame.Jump;
                GameSwimController.SetAnimationJump();
                return;
            }

            if(_enumStateGame == EnumStateGame.Swim)
                GameSwimController.PhysicMovie(InputPosition);
        }

        private void UpdateMoveObjects()
        {
            var positionSwimer = GameSwimController.GetPosition();
            //Debug.Log(moveLevelX + "  " + moveLevelY);
            var target = -CorectPositionInScreenSize(positionSwimer);//new Vector3(-moveLevelX, -moveLevelY, 0));
            var timeUpdate = 0.1f;

            //SwimerSide.GetComponent<RectTransform>().localPosition = Vector3.SmoothDamp(SwimerSide.GetComponent<RectTransform>().localPosition, target, ref _swimerVelocity, timeUpdate);
            Level.GetComponent<RectTransform>().localPosition = Vector3.SmoothDamp(Level.GetComponent<RectTransform>().localPosition, target, ref _levelVelocity, timeUpdate);

            SwimerSide.GetComponent<RectTransform>().localPosition = target;
            //Level.GetComponent<RectTransform>().localPosition = new Vector3(-moveLevelX, -moveLevelY, 0);
        }

        private void UpdateMovieCamera()
        {
            _curTimeUpdateCamera += Time.deltaTime / _timeUpdateCamera;

            if (_curTimeUpdateCamera >= 1.0f)
            {
                _isMovieCamera = false;
                _IsMovieScene = true;
                _curTimeUpdateCamera = 1.0f;

                if (_enumStateGame == EnumStateGame.Swim)
                    GameSwimController.Freeze(false);
            }

            var pos = Vector3.Lerp(_startPositionCamera, _finishPositionCamera, _curTimeUpdateCamera);

            var t = pos;

            
            if (Mathf.Abs(t.x) < _canvasSize.x/2)
                t.x = -_canvasSize.x/2;
            if (Mathf.Abs(t.y) < _canvasSize.y/2)
                t.y = _canvasSize.y/2;

            Level.GetComponent<RectTransform>().localPosition = t;
            SwimerSide.GetComponent<RectTransform>().localPosition = t;
        }

        private void SetupCamera(Vector3 posStart, Vector3 posMovie, float time)
        {
            _timeUpdateCamera = time;
            _startPositionCamera = posStart;
            _finishPositionCamera = posMovie;
            _curTimeUpdateCamera = 0;
            _isMovieCamera = true;
        }

        private Vector3 CorectPositionInScreenSize(Vector3 pos)
        {
            var x = Mathf.Abs(pos.x) < _canvasSize.x / 2 ?
                             _canvasSize.x / 2 : Mathf.Abs(pos.x) > (BackgroundSize.x - _canvasSize.x / 2) ?
                             (BackgroundSize.x - _canvasSize.x / 2) : pos.x;

            var y = Mathf.Abs(pos.y) < _canvasSize.y / 2
                ? -_canvasSize.y / 2
                : Mathf.Abs(pos.y) > (BackgroundSize.y - _canvasSize.y / 2)
                    ? -(BackgroundSize.y - _canvasSize.y / 2)
                    : pos.y;

            return new Vector3(x, y);
        }
    }
}