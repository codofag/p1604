﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace HidenGame
{

    public class ImageParticlePrefab : MonoBehaviour
    {
        public GameObject ParticleStart;
        public GameObject ParticleEnd;

        private List<Vector2> _positionParticle = new List<Vector2>();
        private int _indexStart;
        private int _indexEnd;

        private Vector2 _CurPositionStart;
        private Vector2 _EndPositionStart;

        private Vector2 _CurPositionEnd;
        private Vector2 _EndPositionEnd;

        private float _time = 0;
        private float _speedParticle = 2.0f;

        private bool _pauseParticle = true;

        // Use this for initialization
        void Start()
        {
            InitParticle();
        }

        // Update is called once per frame
        void Update()
        {
            if (_pauseParticle)
                return;

            _time += Time.deltaTime * _speedParticle;

            if (_time >= 1.0f)
            {
                NextParticle();
            }

            SetPositionParticle();
        }

        public void InitParticle()
        {
            _positionParticle.Clear();
            _positionParticle.AddRange(GetComponent<EdgeCollider2D>().points);

            StartParticle();
        }

        void SetPositionParticle()
        {
            var posFirstParticle = Vector3.Lerp(_CurPositionStart, _EndPositionStart, _time);
            var posSecondParticle = Vector3.Lerp(_CurPositionEnd, _EndPositionEnd, _time);

            var position = GetComponent<RectTransform>().localPosition;
            

            ParticleStart.GetComponent<RectTransform>().localPosition = position + posFirstParticle;// + size;
            ParticleEnd.GetComponent<RectTransform>().localPosition = position + posSecondParticle;// + size;
        }

        void StartParticle()
        {
            _pauseParticle = false;

            _indexStart = 0;
            _indexEnd = _positionParticle.Count - 1;

            SetPositionMoveVectror();
        }

        void NextParticle()
        {
            _indexStart++;
            _indexEnd--;

            if (_indexStart >= _indexEnd)
            {
                Debug.Log("END");
                _pauseParticle = true;
                return;
            }

            SetPositionMoveVectror();
        }

        void SetPositionMoveVectror()
        {
            _time = 0;

            _CurPositionStart = _positionParticle[_indexStart];
            _EndPositionStart = _positionParticle[_indexStart + 1];

            _CurPositionEnd = _positionParticle[_indexEnd];
            _EndPositionEnd = _positionParticle[_indexEnd - 1];
        }
    }
}