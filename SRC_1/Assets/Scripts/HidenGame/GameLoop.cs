﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Game.UI;
using SoundManager;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using XMLReader;
using Random = UnityEngine.Random;

namespace HidenGame
{


    public class GameLoop : MonoBehaviour
    {
        public Transform Root;
        public HidenImage PrefabHidenImage;
        public HidenImage PrefabHideHeadset;

        public AnimationManager AnimationManager;

        public GameObject Canvas;

        public GameObject[] Lives;

        private List<Vector3> _randomPositionHeadset = new List<Vector3>();

        private int FindHeadset = 0;

        private Vector3 _positionFirstHeadset = Vector3.zero;

        private float _curGameTime = 0;
        private float _maxGameTime = 40;

        private bool _pauseGame = false;

        private bool _soundWatch = false;


        // Use this for initialization
        void Start()
        {
            Screen.orientation = ScreenOrientation.Landscape;

            var imagesInfo = HideObjectXML.GetAllImages();

            InitMassPosition();

            for (int i = 0; i < 2; i++)
            {
                CreateRandomPositionHeadSet(i + 1);
            }

            foreach (var imageXml in imagesInfo)
            {
                var g = gameObject.AddComponent<DuoImage>();
                g.Init(Root, PrefabHidenImage, imageXml);
                g.transform.SetParent(gameObject.transform);
            }

            AnimationManager.OnActionEndFullAnimation += OnActionEndFullAnimation;

            if (UIManager.Instance != null)
            {
                UIManager.Instance.OnActionPauseGame = OnActionPauseGame;
                UIManager.Instance.CreateUI();
                UIManager.Instance.CreateTypeUI(EnumUIType.BottomSetingButton);
                UIManager.Instance.CreateTypeUI(EnumUIType.Timer);
            }

            if (SoundsManager.Instance != null)
                SoundsManager.Instance.AddSound(gameObject, "HIDEN_START");
        }

        private void OnActionPauseGame(bool pause)
        {
            _pauseGame = pause;
        }

        private void OnActionEndFullAnimation()
        {
            AnimationManager.OnActionEndFullAnimation -= OnActionEndFullAnimation;

            // end game
            EndGame();
        }

        private void InitMassPosition()
        {
            _randomPositionHeadset.Add(new Vector3(320, 890));
            _randomPositionHeadset.Add(new Vector3(659, 992));
            _randomPositionHeadset.Add(new Vector3(705, 880));

            _randomPositionHeadset.Add(new Vector3(552, 868));
            _randomPositionHeadset.Add(new Vector3(404, 974));
            _randomPositionHeadset.Add(new Vector3(177, 1010));

            _randomPositionHeadset.Add(new Vector3(1232, 917));
            _randomPositionHeadset.Add(new Vector3(1700, 978));
            _randomPositionHeadset.Add(new Vector3(1106, 705));

            _randomPositionHeadset.Add(new Vector3(911, 770));
        }

        private void CreateRandomPositionHeadSet(int index)
        {
            var v = GetRandomPosition();
            var go = GameObject.Instantiate(PrefabHideHeadset);
            go.GetComponent<RectTransform>().SetParent(Root);
            go.GetComponent<RectTransform>().localScale = Vector3.one;
            go.Init("headset_" + index, v.x, v.y, true);
            go.OnActionClick = OnClickActionHeadset;
            go.OnDestroyImage = OnDestroyImage;
        }


        private Vector3 GetRandomPosition()
        {
            Vector3 v;
            int index = 0;
            while (true)
            {
                var i = Random.Range(0, _randomPositionHeadset.Count);
                v = _randomPositionHeadset[i];

                if (v != _positionFirstHeadset)
                    break;

                index++;
                if (index >= 10)
                {
                    v = _randomPositionHeadset[0];
                    break;
                }
            }

            if (_positionFirstHeadset == Vector3.zero)
                _positionFirstHeadset = v;

            return v;
        }

        private void OnDestroyImage(GameObject go)
        {
            Destroy(go);
        }

        private void OnClickActionHeadset(GameObject go)
        {
            go.GetComponent<HidenImage>().ShowImage(false);

            Lives[FindHeadset].SetActive(true);

            FindHeadset++;
            if (FindHeadset >= 2)
            {
                _pauseGame = true;
                AnimationManager.WakeUpMisterAnderson();
            }

            if (SoundsManager.Instance != null)
                SoundsManager.Instance.AddSound(gameObject, "HIDEN_HEADSET");
        }

        // Update is called once per frame
        void Update()
        {
            UpdateTime();

            //UpdateInput();
        }

        void UpdateTime()
        {
            if (_pauseGame)
                return;

            _curGameTime += Time.deltaTime;
            if (_curGameTime >= _maxGameTime)
            {
                _curGameTime = _maxGameTime;

                // end game
                EndGame();

                _pauseGame = true;
            }

            // каждые 10 сек запускаем часики
            if (Mathf.FloorToInt(_curGameTime)%10 == 0 &&
                Mathf.FloorToInt(_curGameTime) != 0 &&
                Mathf.Abs(_curGameTime - _maxGameTime) > Mathf.Epsilon && !_soundWatch)
            {
                if (SoundsManager.Instance != null)
                    SoundsManager.Instance.AddSound(gameObject, "HIDEN_WATCH");
                _soundWatch = true;
            }
            else if (Mathf.Abs(_curGameTime - _maxGameTime) < Mathf.Epsilon)
            {
                if (SoundsManager.Instance != null)
                    SoundsManager.Instance.AddSound(gameObject, "HIDEN_ALARM");
            }
            if (Mathf.FloorToInt(_curGameTime)%11 == 0 && Mathf.FloorToInt(_curGameTime) != 0 && _soundWatch)
            {
                _soundWatch = false;
            }

            if (UIManager.Instance != null)
                UIManager.Instance.SetUIValue(EnumUIType.Timer, _curGameTime, _maxGameTime);
        }


        void EndGame()
        {
            // end game
            if (UIManager.Instance != null)
            {
                UIManager.Instance.CreateTypeUI(EnumUIType.EndGameMenu);
                UIManager.Instance.SetUIValue(EnumUIType.EndGameMenu, (int) ((_maxGameTime - _curGameTime)*10));
            }
        }
    }
}