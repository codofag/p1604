﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class HidenImage : MonoBehaviour, IPointerDownHandler
{
    public Action<GameObject> OnActionClick;
    public Action<GameObject> OnDestroyImage;

    private bool _isTimeWork = false;
    private float _time;
    private bool _isHideImage = false;

    private bool _IsClickable = true;

    // Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	    if (!_isTimeWork)
	        return;

	    _time += Time.deltaTime;
	    if (_time >= 1.0f)
	    {
	        _time = 1.0f;
	        _isTimeWork = false;

	        if (_isHideImage)
	        {
	            if (OnDestroyImage != null)
	                OnDestroyImage(gameObject);
	        }
	    }

        var imageSprite = GetComponent<Image>();
        imageSprite.color = new Color(1,1,1, _isHideImage ? 1 - _time : _time );
    }

    public void Init(string nameImage, float x, float y, bool isCenterImage = false)
    {
        var sprite = Resources.Load<Sprite>("Sprites/HidenGame/Objects/" + nameImage);

        var imageSprite = GetComponent<Image>();
        imageSprite.sprite = sprite;
        imageSprite.SetNativeSize();

        var sizeImage = imageSprite.GetComponent<RectTransform>().sizeDelta;
        if(!isCenterImage)
            GetComponent<RectTransform>().localPosition = new Vector3(x + sizeImage.x/2,  -(y + sizeImage.y/2));
        else
            GetComponent<RectTransform>().localPosition = new Vector3(x, -y);

        //GetComponent<BoxCollider>().size = imageSprite.GetComponent<RectTransform>().sizeDelta;
    }

    public void ShowImage( bool show)
    {
        _time = 0.0f;
        _isHideImage = !show;
        _isTimeWork = true;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("click image := " + gameObject.GetComponent<Image>().sprite.name);
        if (!_IsClickable)
            return;

        if (OnActionClick != null)
            OnActionClick(gameObject);
    }

    public void SetClicableImage(bool isClicable)
    {
        _IsClickable = isClicable;
    }
}
