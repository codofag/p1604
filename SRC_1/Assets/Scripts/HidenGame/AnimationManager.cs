﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SoundManager;
using XMLReader;

namespace HidenGame
{
    
    public class AnimationManager : MonoBehaviour
    {
        enum StateAnimation
        {
            Sleep,
            Awake,
            Walk,
            Run1,
            Run2,
        }

        public AnimationController AnimationController;

        public Action OnActionEndFullAnimation;

        List<List<Sprite>> _spritesAnimation;

        private List<NameAnimationXml> _animationXmls = new List<NameAnimationXml>();

        private bool _isWakeUp = false;

        private int _stateAnimation = (int) StateAnimation.Sleep;


        // Use this for initialization
        void Start()
        {
            _spritesAnimation = new List<List<Sprite>>();
            _spritesAnimation.Add(new List<Sprite>(Resources.LoadAll<Sprite>("Sprites/HidenGame/Animation/Sleep/ASleep")));
            _spritesAnimation.Add(new List<Sprite>(Resources.LoadAll<Sprite>("Sprites/HidenGame/Animation/Awake/AAwake")));
            _spritesAnimation.Add(new List<Sprite>(Resources.LoadAll<Sprite>("Sprites/HidenGame/Animation/Walk/AWalk")));
            _spritesAnimation.Add(new List<Sprite>(Resources.LoadAll<Sprite>("Sprites/HidenGame/Animation/Run1/ARun1")));
            _spritesAnimation.Add(new List<Sprite>(Resources.LoadAll<Sprite>("Sprites/HidenGame/Animation/Run2/ARun2")));

            _animationXmls.Clear();
            _animationXmls = AnimationXML.GetAnimation(AnimationXML.FileAnimationHideObject);

            AnimationController.Init(_animationXmls[_stateAnimation].GetFrames(),
                _spritesAnimation[_stateAnimation].ToArray());
            AnimationController.Play();
            AnimationController.OnActionEndAnimation = OnActionEndAnimation;
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnActionEndAnimation()
        {
            if (!_isWakeUp)
            {
                if (SoundsManager.Instance != null)
                    SoundsManager.Instance.AddSound(gameObject, "HIDEN_HRAP");
                return;
            }

            _stateAnimation++;
            if (_stateAnimation > (int) StateAnimation.Run2)
            {
                _stateAnimation = (int) StateAnimation.Run2;

                if (OnActionEndFullAnimation != null)
                    OnActionEndFullAnimation();

                return;
            }

            if (_stateAnimation == (int) StateAnimation.Walk)
            {
                if (SoundsManager.Instance != null)
                    SoundsManager.Instance.AddSound(gameObject, "HIDEN_WALK");
            }
            else if (_stateAnimation == (int) StateAnimation.Run1)
            {
                if (SoundsManager.Instance != null)
                    SoundsManager.Instance.AddSound(gameObject, "HIDEN_RUN");
            }
            else if (_stateAnimation == (int) StateAnimation.Awake)
            {
                if (SoundsManager.Instance != null)
                    SoundsManager.Instance.DelSound(gameObject, "HIDEN_HRAP");
            }

            AnimationController.Init(_animationXmls[_stateAnimation].GetFrames(),
                _spritesAnimation[_stateAnimation].ToArray());
            AnimationController.Play();
        }

        public void WakeUpMisterAnderson()
        {
            _isWakeUp = true;
            OnActionEndAnimation();
        }
    }
}