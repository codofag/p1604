﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

namespace XMLReader
{

    public class AnimationXML
    {
        public const string FileAnimationHideObject = "XML/AnimationXML";
        public const string FileAnimationSwimer = "XML/AnimationSwimerXML";
        //
        private static HidenObjectAnimationXml _Xml;

        public static void Init(string path)
        {
            //if (_Xml == null)
            //{
                var txt = Resources.Load(path) as TextAsset;
                var reader = new StringReader(txt.text);

                var xmlSerializer = new XmlSerializer(typeof (HidenObjectAnimationXml));
                _Xml = xmlSerializer.Deserialize(reader) as HidenObjectAnimationXml;
                reader.Close();
            //}
        }

        public static List<NameAnimationXml> GetAnimation(string path)
        {
            Init(path);

            return _Xml.GetAllAnimations();
        }
    }

    [XmlRoot("HidenObjectAnimation")]
    public class HidenObjectAnimationXml
    {
        [XmlElement("nameAnimation")]
        public List<NameAnimationXml> NameAnimationXmls = new List<NameAnimationXml>();

        public HidenObjectAnimationXml()
        {

        }

        public List<NameAnimationXml> GetAllAnimations()
        {
            return NameAnimationXmls;
        }
    }

    [XmlRoot("nameAnimation")]
    public class NameAnimationXml
    {
        [XmlAttribute("id")]
        public string Name;

        [XmlElement("frameAnimation")]
        public List<FrameAnimationXml> FrameAnimationXmls = new List<FrameAnimationXml>();

        public NameAnimationXml()
        {

        }

        public List<FrameAnimationXml> GetFrames()
        {
            return FrameAnimationXmls;
        }
    }

    [XmlRoot("frameAnimation")]
    public class FrameAnimationXml
    {
        [XmlAttribute("name")]
        public string Name;

        [XmlAttribute("x")]
        public float X;

        [XmlAttribute("y")]
        public float Y;
    }
}