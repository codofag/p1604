﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using XMLReader;

namespace HidenGame
{
    
    public class AnimationController : MonoBehaviour
    {
        public Action OnActionEndAnimation;

        private List<Sprite> _spriteAtlass = new List<Sprite>();

        private int _frame = 0;
        private float _timeFrame = 1.0f/8.0f; // 8 кадров в секунду
        private float _time = 0;

        private bool _isPlayAnimation = false;

        private List<FrameAnimationXml> _frames = new List<FrameAnimationXml>();

        private Vector3 Offset = Vector3.zero;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (!_isPlayAnimation)
                return;

            _time += Time.deltaTime;
            if (_time >= _timeFrame)
            {
                _time = 0;
                NextFrame();
            }
        }

        public void Init(List<FrameAnimationXml> frames, Sprite[] sa)
        {
            _time = 0;
            _isPlayAnimation = false;

            _frames.Clear();
            _frames.AddRange(frames);

            _spriteAtlass.Clear();
            _spriteAtlass.AddRange(sa);

            _frame = -1;

            NextFrame();
        }

        public void SetOffset()
        {
            Offset = new Vector2(int.MaxValue, int.MaxValue);

            foreach (var frameAnimationXml in _frames)
            {
                if (Offset.x > frameAnimationXml.X)
                    Offset.x = frameAnimationXml.X;
                if (Offset.y > frameAnimationXml.Y)
                    Offset.y = frameAnimationXml.Y;
            }

            GetComponent<RectTransform>().localPosition = new Vector3(_frames[_frame].X - Offset.x, -(_frames[_frame].Y - Offset.y), 0);
        }

        private void NextFrame()
        {
            _frame++;

            if (_frame >= _frames.Count)
            {
                if (OnActionEndAnimation != null)
                    OnActionEndAnimation();

                _frame = 0;
            }

            var n = _frames[_frame].Name;
            var s = _spriteAtlass.Find(g => g.name == n);
            if (s != null)
            {
                GetComponent<Image>().sprite = s;
                GetComponent<Image>().SetNativeSize();
            }

            GetComponent<RectTransform>().localPosition = new Vector3(_frames[_frame].X - Offset.x, -(_frames[_frame].Y - Offset.y), 0);
        }

        public void Play()
        {
            _isPlayAnimation = true;
        }

        public void Stop()
        {
            _isPlayAnimation = false;
        }
    }
}