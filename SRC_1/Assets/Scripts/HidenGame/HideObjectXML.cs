﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

namespace XMLReader
{

    public class HideObjectXML
    {

        public const string File = "XML/HidenObject";
        //
        private static HidenObjectXml _Xml;

        public static void Init()
        {
            if (_Xml == null)
            {
                // read from xml
                var txt = Resources.Load(File) as TextAsset;
                var reader = new StringReader(txt.text);

                var xmlSerializer = new XmlSerializer(typeof(HidenObjectXml));
                _Xml = xmlSerializer.Deserialize(reader) as HidenObjectXml;
                reader.Close();
            }
        }

        public static List<ImageXml> GetAllImages()
        {
            Init();

            return _Xml.Get();
        }
    }

    [XmlRoot("HidenObject")]
    public class HidenObjectXml
    {
        [XmlElement("Image")]
        public List<ImageXml> ImageXmls = new List<ImageXml>();

        public HidenObjectXml()
        {

        }

        public List<ImageXml> Get()
        {
            return ImageXmls;
        }
    }

    [XmlRoot("Image")]
    public class ImageXml
    {
        [XmlAttribute("name1")]
        public string Name1;

        [XmlAttribute("x1")]
        public float X1;

        [XmlAttribute("y1")]
        public float Y1;

        [XmlAttribute("name2")]
        public string Name2;

        [XmlAttribute("x2")]
        public float X2;

        [XmlAttribute("y2")]
        public float Y2;
    }
}