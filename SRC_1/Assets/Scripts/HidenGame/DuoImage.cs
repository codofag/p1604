﻿using UnityEngine;
using System.Collections;
using SoundManager;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using XMLReader;

public class DuoImage : MonoBehaviour
{
    private HidenImage _FirstImage = null;
    private HidenImage _SecondImage = null;

	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    public void Init(Transform root, HidenImage prefab, ImageXml imageInfo)
    {
        if (imageInfo.Name1.Length < 1)
            return;

        _FirstImage = GameObject.Instantiate(prefab);
        _FirstImage.GetComponent<RectTransform>().SetParent(root);
        _FirstImage.GetComponent<RectTransform>().localScale = Vector3.one;
        _FirstImage.Init(imageInfo.Name1, imageInfo.X1, imageInfo.Y1);
        _FirstImage.OnActionClick = OnClickAction;
        _FirstImage.OnDestroyImage = OnDestroyImage;

        if (imageInfo.Name2 != null)
        {
            _SecondImage = GameObject.Instantiate(prefab);
            _SecondImage.GetComponent<RectTransform>().SetParent(root);
            _SecondImage.GetComponent<RectTransform>().localScale = Vector3.one;
            _SecondImage.Init(imageInfo.Name2, imageInfo.X2, imageInfo.Y2);
            _SecondImage.GetComponent<Image>().color = new Color(1,1,1,0);

            _SecondImage.SetClicableImage(false);
            _SecondImage.OnActionClick = OnClickAction;
            _SecondImage.OnDestroyImage = OnDestroyImage;
        }
        else
        {
            //DisablePinter(_FirstImage);
            _FirstImage.SetClicableImage(false);
        }
    }

    private void OnDestroyImage(GameObject go)
    {
        var image = go.GetComponent<HidenImage>();
        if (image == _FirstImage)
            _FirstImage = null;
        else
            _SecondImage = null;
        
        Destroy(go);
    }

    public void OnClickAction(GameObject go)
    {
        if (_SecondImage != null)
        {
            _SecondImage.ShowImage(true);

            if (SoundsManager.Instance != null)
                SoundsManager.Instance.AddSound(gameObject, "HIDEN_CLICK_OBJECT");
        }

        if (_FirstImage != null)
        {
            _FirstImage.ShowImage(false);
            _FirstImage.SetClicableImage(false);
        }
    }
}
