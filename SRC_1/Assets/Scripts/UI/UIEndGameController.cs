﻿using UnityEngine;
using System.Collections;
using UI.Game;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIEndGameController : MonoBehaviour
{
    public GameObject Root;

    public UIButtonController EndGame;
    public UIButtonController SaveGame;

    public Text Scores;

    public RegistrationFormaController VRegistrationFormaController;
    public RegistrationFormaController HRegistrationFormaController;

	// Use this for initialization
	void Start ()
    {
	    EndGame.OnActionClick = delegate(GameObject o)
	    {
	        SceneManager.LoadScene("Scenes/MainMenu");
	    };

        SaveGame.OnActionClick = delegate(GameObject o)
        {
            if (Screen.width < Screen.height)
            {
                VRegistrationFormaController.Init();
                VRegistrationFormaController.gameObject.SetActive(true);
            }
            else
            {
                HRegistrationFormaController.Init();
                HRegistrationFormaController.gameObject.SetActive(true);
            }
        };
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    public void SetValue(int scores)
    {
        Scores.text = "" + scores;
    }

    public void Show()
    {
        Root.SetActive(true);
    }
}
