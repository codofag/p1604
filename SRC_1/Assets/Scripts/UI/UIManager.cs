﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SoundManager;
using UnityEngine.SceneManagement;
using Object = System.Object;

namespace Game.UI
{
    
    public class UIManager : MonoBehaviour
    {
        public static UIManager Instance;

        private UIController _uiController;
        private List<EnumUIType> _listTypeui = new List<EnumUIType>();

        public Action<bool> OnActionPauseGame; 

        public void Init()
        {
            //Debug.LogError("Init sound manager");

            if (Instance == null)
            {
                Instance = this;
            }

            //SceneManager.activeSceneChanged += OnSceneWasSwitched;
            SceneManager.sceneLoaded += OnSceneWasChanged;
        }

        private void OnDestroy()
        {
            SceneManager.sceneLoaded -= OnSceneWasChanged;
        }

        private void OnSceneWasChanged(Scene scene, LoadSceneMode mode)
        {
            if (mode != LoadSceneMode.Single)
                return;

            //Debug.Log("scene := " + scene.name + " mode := " + mode);

            OnActionPauseGame = null;
        }

        public void InitUIController(UIController uiController)
        {
            _uiController = uiController;

            foreach (var enumUiType in _listTypeui)
            {
                _uiController.AddUI(enumUiType);
            }
        }

        public void PauseGame(bool pause)
        {
            if (OnActionPauseGame != null)
                OnActionPauseGame(pause);
        }
        
        public void CreateUI()
        {
            SceneManager.LoadScene("Scenes/UI/GameUI", LoadSceneMode.Additive);

            _listTypeui.Clear();
        }

        public void CreateTypeUI(EnumUIType type)
        {
            if (_uiController == null)
            {
                _listTypeui.Add(type);  
                return;
            }

            _uiController.AddUI(type);
        }

        public void SetUIValue(EnumUIType type, Object a = null, Object b = null)
        {
            if(_uiController != null)
                _uiController.SetUI(type, a, b);
        }
    }

}