﻿using UnityEngine;
using System.Collections;
using UI.Game;

public class UIButtonSettingController : MonoBehaviour
{
    public UISettingController UiSettingController;
    public UIButtonController SettingButtonController;

	// Use this for initialization
	void Start ()
    {
	    SettingButtonController.OnActionClick = delegate(GameObject o )
	    {
	        UiSettingController.Show();
	    };
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Show()
    {
        gameObject.SetActive(true);
    }
}
