﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UITimeController : MonoBehaviour
{
    public GameObject Root;

    public Canvas Canvas;

    public Image Background;
    public Image Filler;
    public Image Filler2;

    public Text TextTime;

    //public float TimeToFiler = 0;

    // Use this for initialization
	void Start ()
    {
        
    }
	
	// Update is called once per frame
	void Update ()
    { 
	}

    public void Show()
    {
        Root.SetActive(true);

        var s = Canvas.gameObject.GetComponent<RectTransform>().sizeDelta;
        s.x -= Background.GetComponent<RectTransform>().localPosition.x * 2;
        Background.GetComponent<RectTransform>().sizeDelta = new Vector2(s.x, Background.GetComponent<RectTransform>().sizeDelta.y);

        Debug.Log("size := " + s.x);
    }

    public void SetTime(float curTime, float maxTime)
    {
        var p = curTime / maxTime;

        var size = Background.GetComponent<RectTransform>().sizeDelta;
        
        Filler.GetComponent<RectTransform>().sizeDelta = new Vector2(size.x * p, size.y);
        Filler2.GetComponent<RectTransform>().localPosition = new Vector3(-10,10,0) + new Vector3(Filler.GetComponent<RectTransform>().sizeDelta.x, 0, 0);

        TextTime.text = "" + Mathf.FloorToInt(curTime) + "\" / " + Mathf.FloorToInt(maxTime) + "\"";
    }
}
