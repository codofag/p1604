﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace ShootGame
{
    public class DestroyScene : MonoBehaviour
    {
        private bool _destroyScene = false;
        //public string NameScene;

        // Use this for initialization
        void Start()
        {
            //SceneManager.MoveGameObjectToScene(gameObject, SceneManager.GetActiveScene());
            //Debug.Log("active scene := " + gameObject.scene.name);
        }

        public void UnloadScene()
        {
            _destroyScene = true;
        }

        // Update is called once per frame
        protected virtual void Update()
        {
            if (_destroyScene)
            {
                _destroyScene = false;
                SceneManager.UnloadScene(gameObject.scene.name);
            }
        }
    }
}