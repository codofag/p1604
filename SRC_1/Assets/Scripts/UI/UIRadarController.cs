﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ShootGame;
using UnityEngine.UI;

public class UIRadarController : MonoBehaviour
{
    public GameObject Root;

    public GameObject RootRadarPoint;
    public GameObject RadarPoint;

    public Image Background;

    public Image CameraView;

    private readonly List<GameObject> _devilPoints = new List<GameObject>();

    // Use this for initialization
    void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	    var a = Background.GetComponent<RectTransform>().localEulerAngles;
	    a.z -= Time.deltaTime * 20;
	    Background.GetComponent<RectTransform>().localEulerAngles = a;
	}

    public void Show()
    {
        Root.SetActive(true);

        RadarPoint.SetActive(false);

        for (int i = 0; i < 20; i++)
        {
            var go = Instantiate(RadarPoint);
            go.transform.SetParent(RootRadarPoint.transform);
            
            _devilPoints.Add(go);
        }

        _devilPoints.Add(RadarPoint);

        foreach (var devilPoint in _devilPoints)
        {
            devilPoint.GetComponent<RectTransform>().sizeDelta = new Vector2(16, 16);
            devilPoint.GetComponent<RectTransform>().localScale = Vector3.one;
        }
    }

    public void UpdatePosition(GameObject cam, List<Devils> devils)
    {
        CameraView.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0,
            -cam.GetComponent<Transform>().eulerAngles.y);

        for (int i = 0; i < _devilPoints.Count; i++)
        {
            if (devils.Count > i)
            {
                if (devils[i].GetLifeCycleDevil() != LifeCycleDevils.Life)
                {
                    _devilPoints[i].SetActive(false);
                    continue;
                }
                else
                {
                    var x = devils[i].transform.position.x * 12.5f;
                    var y = devils[i].transform.position.z * 12.5f;

                    _devilPoints[i].SetActive(true);
                    _devilPoints[i].GetComponent<RectTransform>().localPosition = new Vector3(x, y, 0);

                }
            }
            else
            {
                _devilPoints[i].SetActive(false);
            }
        }
    }
}
