﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UI.Game;
using UnityEngine.UI;

public class RegistrationFormaController : MonoBehaviour
{
    public UIButtonController SaveButtonController;

    public InputField NameInputFieldController;
    public InputField EMailInputFieldController;

    public GameObject YouDateSave;

    public UIButtonController YouCollegeButtonController;
    public GameObject RootList;
    public UIButtonController[] CollegesButtonController;
    
	// Use this for initialization
	void Start ()
    {
	    YouCollegeButtonController.OnActionClick = delegate(GameObject o)
	    {
	        RootList.SetActive(true);
	    };

	    foreach (var uiButtonController in CollegesButtonController)
	    {
	        uiButtonController.OnActionClick = OnActionYouCollege;
	    }

        SaveButtonController.OnActionClick = delegate(GameObject o)
        {
            YouDateSave.SetActive(true);
        };

        YouDateSave.SetActive(false);
        RootList.SetActive(false);

        SaveButtonController.OnActionClick = delegate (GameObject o)
        {
            gameObject.SetActive(false);
        };
    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    public void Init()
    {
        YouCollegeButtonController.GetComponentInChildren<Text>().text = "Ваш ВУЗ";
        NameInputFieldController.text = "";
        EMailInputFieldController.text = "";
    }

    void OnActionYouCollege(GameObject go)
    {
        YouCollegeButtonController.GetComponentInChildren<Text>().text = go.GetComponentInChildren<Text>().text;
        RootList.SetActive(false);
    }
}
