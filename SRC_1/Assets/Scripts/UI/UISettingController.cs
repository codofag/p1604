﻿using System;
using UnityEngine;
using System.Collections;
using Game.UI;
using UI.Game;
using UnityEngine.SceneManagement;

public class UISettingController : MonoBehaviour
{
    public GameObject Root;

    public UIButtonController SoundOffButtonController;
    public UIButtonController SoundOnButtonController;
    public UIButtonController ExitButtonController;
    public UIButtonController ContinueButtonController;

	// Use this for initialization
	void Start ()
    {
	    SoundOffButtonController.OnActionClick = OnActionSoundButtonClick;
        SoundOnButtonController.OnActionClick = OnActionSoundButtonClick;

        ExitButtonController.OnActionClick = delegate(GameObject o)
        {
            SceneManager.LoadScene("Scenes/MainMenu", LoadSceneMode.Single);
        };

        ContinueButtonController.OnActionClick = delegate(GameObject o)
        {
            if (UIManager.Instance != null)
                UIManager.Instance.PauseGame(false);

            Root.SetActive(false);
        };
	}

    private void OnActionSoundButtonClick(GameObject go)
    {
        Debug.Log("get sound := " + AccountPrefs.GetSound() + " name := " + go.name);

        if (go.GetComponent<UIButtonController>() == SoundOffButtonController)
        {
            AccountPrefs.SetSound(false);
        }
        else
        {
            AccountPrefs.SetSound(true);
        }

        SoundOffButtonController.gameObject.SetActive(AccountPrefs.GetSound());
        SoundOnButtonController.gameObject.SetActive(!AccountPrefs.GetSound());

        AccountPrefs.Save();
    }

    // Update is called once per frame
	void Update ()
    {
	
	}

    public void Show()
    {
        Root.SetActive(true);

        SoundOffButtonController.gameObject.SetActive(AccountPrefs.GetSound());
        SoundOnButtonController.gameObject.SetActive(!AccountPrefs.GetSound());

        if (UIManager.Instance != null)
            UIManager.Instance.PauseGame(true);
    }
}
