﻿using System;
using UnityEngine;
using System.Collections;
using SoundManager;
using UnityEngine.EventSystems;

namespace UI.Game
{


    public class ButtonController : MonoBehaviour, IPointerDownHandler
    {
        public Action<GameObject> OnActionClick;

        // Use this for initialization
        private void Start()
        {

        }

        // Update is called once per frame
        private void Update()
        {

        }

        public void OnPointerDown(PointerEventData eventData)
        {
            //Debug.Log("Click");

            if (SoundsManager.Instance != null)
                SoundsManager.Instance.AddSound(gameObject, "CLICK");

            if (OnActionClick != null)
                OnActionClick(gameObject);
        }
    }
}