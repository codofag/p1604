﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Game.UI;
using ShootGame;
using Object = System.Object;

public class UIController : MonoBehaviour
{
    public UIButtonSettingController BottomButtonSettingController;
    public UIButtonSettingController UpLeftButtonSettingController;
    public UIButtonSettingController UpRightButtonSettingController;
    public UIEndGameController UiEndGameController;
    public UITimeController UiTimeController;
    public UIRadarController UiRadarController;

	// Use this for initialization
	void Start ()
    {
	    if(UIManager.Instance != null)
            UIManager.Instance.InitUIController(this);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void AddUI(EnumUIType type)
    {
        switch (type)
        {
            case EnumUIType.BottomSetingButton:
                BottomButtonSettingController.Show();
                break;
            case EnumUIType.UpLeftSetingButton:
                UpLeftButtonSettingController.Show();
                break;
            case EnumUIType.UpRightSettinfButton:
                UpRightButtonSettingController.Show();
                break;
            case EnumUIType.EndGameMenu:
                UiEndGameController.Show();
                break;
            case EnumUIType.Timer:
                UiTimeController.Show();
                break;
            case EnumUIType.Radar:
                UiRadarController.Show();
                break;
        }
    }

    public void SetUI(EnumUIType type, Object a, Object b)
    {
        if (type == EnumUIType.EndGameMenu)
        {
            UiEndGameController.SetValue((int)a);
        }
        else if (type == EnumUIType.Timer)
        {
            UiTimeController.SetTime((float)a,(float)b);
        }
        else if (type == EnumUIType.Radar)
        {
            UiRadarController.UpdatePosition((GameObject)a, (List<Devils>)b);
        }
    }
}
