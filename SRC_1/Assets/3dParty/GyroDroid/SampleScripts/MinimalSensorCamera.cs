using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MinimalSensorCamera : MonoBehaviour
{
    // Use this for initialization
    void Start () {
		// you can use the API directly:
		// Sensor.Activate(Sensor.Type.RotationVector);
		
		// or you can use the SensorHelper, which has built-in fallback to less accurate but more common sensors:
		SensorHelper.ActivateRotation();

		useGUILayout = false;
	}
	
	// Update is called once per frame
	void Update () {
        // direct Sensor usage:
        // transform.rotation = Sensor.rotationQuaternion; --- is the same as Sensor.QuaternionFromRotationVector(Sensor.rotationVector);

// Helper with fallback:

#if !UNITY_EDITOR
        transform.eulerAngles = new Vector3(0, Sensor.rotation.eulerAngles.y, 0);
#else
	    if (Input.GetMouseButton(1))
	    {
	        float v = Input.mousePosition.x > Screen.width / 2.0f ? 1 : -1;
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + v * 1.0f, 0);
	    }
#endif
    }
}