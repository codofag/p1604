﻿using UnityEngine;
using System.Collections;
using Game.UI;
using SoundManager;
using UnityEngine.SceneManagement;


namespace LinesGame
{
    public class StartUp : MonoBehaviour
    {
        public string LoadLevelAfter = "Scenes/MainMenu";

        // Use this for initialization
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);

            var sound = gameObject.AddComponent<SoundsManager>();
            sound.Init();

            var ui = gameObject.AddComponent<UIManager>();
            ui.Init();

            SceneManager.LoadScene(LoadLevelAfter, LoadSceneMode.Single);
        }

        // Update is called once per frame
        private void Update()
        {

        }
    }
}