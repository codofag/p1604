﻿Shader "ShooterGame/DiffuseMobileColor" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)

		_SpecColor("Specular Color", Color) = (1,1,1,1)
		_SpecPower("Specular Power", Range(0,1)) = 0.5
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		
		#pragma surface surf BlinnPhong

		fixed4 _Color;
		float _SpecPower;

		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			
			o.Specular = _SpecPower;
			o.Gloss = _Color.a;
			o.Albedo = _Color.rgb;
			//o.Alpha = _Color.a;
		}
		ENDCG
	}

	FallBack "Specular"
}
