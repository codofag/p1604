﻿Shader "Custom/MobileDisolveShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_ColorDisolve ("Color Disolve", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}

		_DissolvePercentage("Dissolve Percentage", Range(0,1)) = 0.0

		_SpecColor("Specular Color", Color) = (1,1,1,1)
		_SpecPower("Specular Power", Range(0,1)) = 0.5
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		Cull Off
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf BlinnPhong

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 2.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _DissolvePercentage;
		float _SpecPower;
		//half _Metallic;
		fixed4 _Color;
		fixed4 _ColorDisolve;


		void surf (Input IN, inout SurfaceOutput o) {
			// Albedo comes from a texture tinted by color
			half gradient = tex2D(_MainTex, IN.uv_MainTex).r;// *_Color;
			clip(gradient - _DissolvePercentage);

			if (gradient < _DissolvePercentage + 0.05)
			{
				o.Albedo = float4(0.0, 0.8, 1.0, _Color.a);
				o.Emission = _ColorDisolve.rgb;
			}
			else
			{
				o.Albedo = _Color.rgb;
				o.Emission = float3(0, 0, 0);
			}

			//o.Albedo = gradient * _Color.rgb;
			// Metallic and smoothness come from slider variables
			//o.Metallic = _Metallic;
			//o.Smoothness = _Glossiness;
			//o.Alpha = c.a;
			o.Specular = _SpecPower;
			o.Gloss = _Color.a;
		}
		ENDCG
	}
	FallBack "Specular"
}
