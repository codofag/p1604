﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using Object = UnityEngine.Object;

public class FrameAnimation
{
    public FrameAnimation(int x, int y, string frame)
    {
        X = x;
        Y = y;
        FrameName = frame;
    }

    public int X;
    public int Y;
    public string FrameName;
}

public class AlphaCrop : EditorWindow
{
    //private List<Object> _objects = new List<Object>();

    private List<FrameAnimation> _frameAnimations = new List<FrameAnimation>();


	[MenuItem("Window/Alpha Crop")]

    // Update is called once per frame
    private static void Init()
    {
        GetWindow<AlphaCrop>(false, "Alpha Crop");
    }

    void OnGUI()
    {
        if (GUILayout.Button("Crop"))
            CropImage();
        if (GUILayout.Button("Crop Animation"))
            CropImageAnimation();
        if (GUILayout.Button("SaveAnimationFile"))
        {
            SaveAnimationFile();
        }
        if (GUILayout.Button("Clear Information Animation"))
        {
            Clear();
        }
    }

    void OnSelectionChange()
    {
    }

    void CropImage()
    {
        var path = Application.dataPath + "/Resources/TempFolder/";
        if (!Directory.Exists(path))
            Directory.CreateDirectory(path);

        string xmlFile = "<Image ";

        int index = 1;

        var objs = Selection.objects.ToList();
        objs.Sort((f1,f2) => String.Compare(f1.name, f2.name, StringComparison.Ordinal));
        

        foreach (var o in objs)
        {
            var texture2D = o as Texture2D;  
            var rect = GetAlphaRect(texture2D);

            int x = (int)rect.x;
            int y = (int)rect.y;
            int w = (int)rect.width;
            int h = (int)rect.height;
            int th = texture2D.height - h;

            //Debug.Log("x = " + x + " y = " + y + " w = " + w + " h = " + h + " xw = " + (w - x) + " yh = " + (h - y));
            
            var texture = new Texture2D(w - x, h - y);
            texture.SetPixels(texture2D.GetPixels(x, y, w - x, h - y));
            texture.Apply();

            var bytes = texture.EncodeToPNG();

            if (File.Exists(path + texture2D.name + ".png"))
                File.Delete(path + texture2D.name + ".png");
            File.WriteAllBytes(path + texture2D.name + ".png", bytes);

            xmlFile += " name" + index + "=\"" + texture2D.name + "\" x"+ index + "=\"" + x +"\" y" + index + "=\"" + th + "\"";
            index++;
        }

        xmlFile += " />";

        Debug.Log(xmlFile);
        
        AssetDatabase.Refresh();
    }

    void CropImageAnimation()
    {
        var path = Application.dataPath + "/Resources/TempFolderAnimation/";
        if (!Directory.Exists(path))
            Directory.CreateDirectory(path);

        var objs = Selection.objects.ToList();
        objs.Sort((f1, f2) => String.Compare(f1.name, f2.name, StringComparison.Ordinal));

        foreach (var o in objs)
        {
            var texture2D = o as Texture2D;
            var rect = GetAlphaRect(texture2D);

            int x = Mathf.FloorToInt(rect.x);
            int y = Mathf.FloorToInt(rect.y);
            int w = Mathf.FloorToInt(rect.width);
            int h = Mathf.FloorToInt(rect.height);
            int th = texture2D.height - h;

            var texture = new Texture2D(w - x, h - y);
            texture.SetPixels(texture2D.GetPixels(x, y, w - x, h - y));
            texture.Apply();

            var bytes = texture.EncodeToPNG();

            if (File.Exists(path + texture2D.name + ".png"))
                File.Delete(path + texture2D.name + ".png");
            File.WriteAllBytes(path + texture2D.name + ".png", bytes);

            AddNewFrame(texture2D.name, x, th);
        }

        AssetDatabase.Refresh();
    }

    Rect GetAlphaRect(Texture2D tex)
    {
        var w = tex.width;
        var h = tex.height;

        int minI = w;
        int minJ = h;

        int maxI = 0;
        int maxJ = 0;

        for (int j = 0; j < h; ++j)
        {
            for (int i = 0; i < w; ++i)
            {
                if (tex.GetPixel(i,j).a > 0)
                {
                    if (minJ > j)
                    {
                        minJ = j;
                    }

                    if (minI > i)
                        minI = i;

                    if (maxI < i)
                        maxI = i;

                    if (maxJ < j)
                        maxJ = j;
                }
            }
        }

        maxJ++;
        maxI++;

        return new Rect(minI, minJ, maxI, maxJ);
    }

    void Clear()
    {
        _frameAnimations.Clear();
    }

    void AddNewFrame(string name, int x, int y)
    {
        if (_frameAnimations.Any(f => f.FrameName == name))
            return;

        _frameAnimations.Add(new FrameAnimation(x, y, name));
    }

    void SaveAnimationFile()
    {
        var path = Application.dataPath + "/Resources/TempFolderAnimation/";

        string xmlFile = "<?xml version=\"1.0\"?>\n\n" +
                         "<HidenObjectAnimation>\n";
        xmlFile += "    <nameAnimation id=\"none\">\n";

        _frameAnimations.Sort( (f1,f2)=> String.Compare(f1.FrameName, f2.FrameName, StringComparison.Ordinal));

        foreach (var frameAnimation in _frameAnimations)
        {
            xmlFile += "        <frameAnimation name=\"" + frameAnimation.FrameName + "\" x=\"" + frameAnimation.X + "\" y=\"" + frameAnimation.Y + "\" />\n";
        }

        xmlFile += "    </nameAnimation>\n";
        xmlFile += "</HidenObjectAnimation>";
        File.WriteAllText(path + "AnimationXML.xml", xmlFile);

        AssetDatabase.Refresh();
    }
}
